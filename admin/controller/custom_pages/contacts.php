<?php
class ControllerCustomPagesContacts extends Controller { 
	private $error = array();

	public function index() {
		 
		$this->load->model('custom_pages/contacts');

		$this->update();
	}

	public function update() {
		$this->language->load('catalog/custom_pages');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('custom_pages/contacts');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) 
		{
			$contacts['contacts_adress'] = html_entity_decode($this->request->post['contacts_adress']);
			$contacts['contacts_info'] = html_entity_decode($this->request->post['contacts_info']);
			$contacts['contacts_requisits'] = html_entity_decode($this->request->post['contacts_requisits']);

			$this->model_custom_pages_contacts->editContacts($contacts);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('custom_pages/contacts', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_seo_title'] = $this->language->get('entry_seo_title');
		$this->data['entry_seo_h1'] = $this->language->get('entry_seo_h1');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}
		
	 	if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}
		
		$url = '';
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => "Контакты",
			'href'      => $this->url->link('custom_pages/contacts', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		// Только update
		$this->data['action'] = $this->url->link('custom_pages/contacts/update', 'token=' . $this->session->data['token'], 'SSL');
				
		$this->data['cancel'] = $this->url->link('custom_pages/contacts', 'token=' . $this->session->data['token'] . $url, 'SSL');

		// 
		// Обновляем поля из запроса
		// 

		if (isset($this->request->post['contacts_adress'])) 
		{
			$this->data['contacts_adress'] = $this->request->post['contacts_adress'];
			$this->data['contacts_info'] = $this->request->post['contacts_info'];
			$this->data['contacts_requisits'] = $this->request->post['contacts_requisits'];
		}
		elseif (!isset($this->request->get['contacts_adress'])) 
		{	
			$contacts = $this->model_custom_pages_contacts->getContacts();
			$this->data['contacts_adress'] = $contacts['adress'];
			$this->data['contacts_info'] = $contacts['info'];
			$this->data['contacts_requisits'] = $contacts['requisits'];
		} else 
		{
			$this->data['contacts_adress'] = '';
			$this->data['contacts_info'] = '';
			$this->data['contacts_requisits'] = '';
		}

		$this->data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->template = 'custom_pages/contacts.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'custom_pages/contacts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>