<?php  
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		
		// Выводим новости на главной
		$this->load->model('catalog/news');
		$this->load->model('tool/image');

		$news_data = $this->model_catalog_news->getNews();
		$news_num = 0;
		foreach ($news_data as $result) 
		{
			// Не больше 4 по дизайну
			if ($news_num == 4) break;
			$this->data['news_data'][] = array(
				'id'  				=> $result['news_id'],
				'title'        		=> $result['title'],
				'thumb'				=> $this->model_tool_image->resize($result['image'], $this->config->get('news_thumb_width'), $this->config->get('news_thumb_height')),
				'description'  	=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $chars),
				'href'         		=> $this->url->link('information/news', 'news_id=' . $result['news_id']),
				'posted'   		=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
			$news_num++;
		}

		// Вывод ссылок на категории
		$this->load->model('catalog/category');

		$this->data['categories'] = array();
			
			$results = $this->model_catalog_category->getCategories();
			
			foreach ($results as $result) 
			{
				$this->data['categories'][] = array(
					'href'  => $this->url->link('product/category', 'path=' . $result['category_id']),
					'thumb' => $this->model_tool_image->resize(($result['image']=='' ? 'no_image.jpg' : $result['image']), 484, 251)
				);
			}

		$this->data['heading_title'] = $this->config->get('config_title');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'module/banner',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>