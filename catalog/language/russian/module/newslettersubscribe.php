<?php
// Heading 
$_['heading_title'] 	 = 'Подпишитесь на рассылку новостей:';

//Fields
$_['entry_email'] 		 = 'Введите Ваш e-mail';
$_['entry_name'] 		 = 'Name';

//Buttons
$_['entry_button'] 		 = 'Подписаться';
$_['entry_unbutton'] 	 = 'Отписаться';

//text
$_['text_subscribe'] 	 = 'Подписаться здесь';

$_['mail_subject']   	 = 'Новости от Shtorykiseya';

//Error
$_['error_invalid'] 	 = 'Неверный e-mail';

$_['subscribe']	    	 = 'Подписаны!';
$_['unsubscribe'] 	     = 'Отписаны!';
$_['alreadyexist'] 	     = 'Такой уже есть';
$_['notexist'] 	    	 = 'Нет такого e-mail';

?>