(function ($) {

	window.SITE = {
		init : function () {
			$("a.gallery").lightbox();
			this.slideMenu();
			this.slideHeader();
			$("#slider").slider();
			$(".carus").carusel({items:5, padding:5, pagination:true, item_content:'.sm_content2'});
			$(".small_car").carusel({items:10, padding:5, item_content:'.sm_content'});
			this.clearResize();
			this.popupInit();
			this.init_quan();
			return this;


		},
		init_quan : function () {
			$(".quan").find('a').click(function(event){
				var input = $(this).parent().find('input'),
				    val = 1,
				    min = input.attr('min');
				
				if ( $(this).attr('class').match('tp') ) { 
					val =  (input.val() * 0 != 0) ? 1 :  input.val()*1 + 1;
				} else {
					val =  (input.val() * 0 != 0  || input.val()*1  < 1 ) ? 0 :  input.val()*1 - 1;
				}
				
				if (typeof min != 'undefined' && val < min) {
					val = min;
				}

				input.val(val).change();
				
				return false;
			});
		},
		popupInit : function () {
			$(document).on('click touchstart','*', function(event) {
				// event.preventDefault();

				var _this,
				    popup,
				    left,
				    top
				    ;
	
				_this = $(event.currentTarget);

				if ( $(_this).attr('class') && $(_this).attr('class').match(/popup/gi) ) {
					$('.popup').hide();
					left  = $(_this).offset().left;
					top   = $(_this).offset().top;
					popup = $(event.currentTarget).attr('data-popup');
					var padding =  ($(_this).css('padding-left').match(/\d+/gi).toString() * 1 ) + ($(_this).css('padding-right').match(/\d+/gi).toString() * 1 );
					var lefter = ( ( left - $(popup).width() + $(_this).width() + padding ) < 0 ) ?  5 :  ( left - $(popup).width() + $(_this).width() + padding );
					
					$(popup).css({'left':lefter, 'top':top + $(_this).height() + 10 });
					$(popup).show();
					
					return false;
				} else {
					$('.popup').hide();
				}
			
					
			});
			
			$('.popup').not("a").on('click touchstart',function(event)
			{
				if ($(event.target).prop("tagName") == "A") { event.stopPropagation(); return true; } 
				if (this.id == "log")
				{
					return true;
				}
				return false;
			});
			
			// $('a').off('click');
			$('.popup').find('.popupExit').on('click touchstart',function(){
				$('.popup').hide();
			});
		},
		slideMenu : function () {
			var _this = this,
			    el = $(".hide_menu"),
			    menu   = $(".top_menu"),
			    block = false;
			
			$(el).on('click touchstart',function(event){
				if (event.preventDefault) {
					event.preventDefault();
				}
				
				if (block) {
					return false;
				} 
				
				block = true;

				if ( $(this).attr('class').match(/active/gi) ) {
					$(menu).slideUp(400,function () {
						block = false;
					});
					$(el).removeClass('active');
				} else {
					$(menu).slideDown(400,function () {
						block = false;
					});
					$(el).addClass('active');
				}

				
			});

			$(menu).find('.select').on('click', function(event){
				
				event.preventDefault();

				if ( $(this).children().attr('class').match(/active/gi) ) {
					$(this).parent().parent().find('ul').slideUp(400,function () {
						block = false;
					});
					$(this).children().removeClass('active');
				} else {
					$(this).parent().parent().find('ul').slideDown(400,function () {
						block = false;
					});
					$(this).children().addClass('active');
				}
			});
		},
	
		slideHeader : function () {
			var _this = this,
			    el = $(".hide_header"),
			    f_box   = $(".first_box"),
			    s_box   = $(".second_box"),
			    block = false;
			
			$(el).on('click touchstart',function(event){
				if (event.preventDefault) {
					event.preventDefault();
				}
				
				if (block) {
					return false;
				} 
				
				block = true;

				if ( $(this).children().attr('class').match(/active/gi) ) {
					$(f_box).slideUp(400,function () {
						block = false;
					});
					$(s_box).slideDown(400);

					$(el).children().removeClass('active');
				} else {
					$(f_box).slideDown(400,function () {
						block = false;
					});

					$(s_box).slideUp(400);
					$(el).children().addClass('active');
				}

				
			});
		}, 
		clearResize : function () {
			$(window).resize(function(event){
				if ( $(document).width() > 700) {
					 $(".resize").removeAttr('style');
				}
			});
		}
	}

})(jQuery);

jQuery(document).ready(function ($) {
	SITE.init();
});