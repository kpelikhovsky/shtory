(function($){
	$.fn.slider = function () {
		$(this).each(function (i,el) {

			var _this = el,
			    width  = 970;
			    slides = $(_this).find(".item"),
			    len = $(slides).length,
			    sl_content = $(_this).find(".sl_content"),
			    sl_nav  = $(_this).find('.sl_nav'),
			    cur_id = 0,
			    block = false;
				
			setWidth();
			renderNav();
			$(window).resize(function(){
				setWidth();
				
				if (block) { 
					return false
				}

				block = true;

				trigger(cur_id,true);
			});	

			function setWidth() {
				width = $(_this).width();
				$(slides).css('width',width);
			}

			function trigger(i,val) {
			
				cur_id = ( i < len) ?   i : 0;

				$(sl_nav).find('.active').removeClass('active');
				$($(sl_nav).children()[i]).addClass('active');

				if (val) {
					$(sl_content).css({'margin-left' : -(i * width) });
					block =false;
				} else {
					$(sl_content).animate({'margin-left' : -(i * width) }, 400, function(){
						block =false;
					});
				}
			}


			function renderNav() {
				var i = len;
				
				while(i) {
					i = i -1;
					$(sl_nav).prepend("<span data-id="+i+"></span>");
				}

				$(sl_nav).children().first().addClass('active');
				
				$(sl_nav).on('click touchstart','span', function(event){
					
					if (block) {
						return false;
					}

					block = true;
					id = $(event.currentTarget).attr('data-id');

					if (!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
		     			event.preventDefault();
		     		}

					trigger(id);
				});

				$(_this).on('click touchstart', 'a[class^=nav_a]', function (event) {
					
					event.preventDefault();
					
					if (block) {
						return false;
					}

					block = true;

					if ( $(event.currentTarget).attr('class').match(/right/gi) ) {
						cur_id =  (cur_id < len-1) ?  cur_id*1 + 1 : 0;
						trigger(cur_id);
					} else {
						cur_id =  (cur_id == 0) ?  len-1 : cur_id*1 -1;
						trigger(cur_id);
					}

				});
			}
		});
	}

	$.fn.carusel = function (json) {
		
		var d_json = {
		        items : 5,
		        padding : 12,
		        item_content : '.item_content',
		        pagination: false
		    },

		    n_json = $.extend(d_json,json);
		
		$(this).each(function (i,el) {
			var _this = el,
			    sl_width = 0,
			    min_space = 5,
			    slides = $(_this).find(".item"),
			    collection = $(_this).find(".hide_items"),
			    len = $(slides).length,
			    car_content = $(_this).find(".car_content"),
			    nav_link = $(_this).find('.nav_a'),
			    cur_id = 0,
			    block = false,
				size = 1,
				c_size = 0,
				pagin  = 1;
				
			if (!n_json.pagination) {
				$(_this).find('.pagination').hide();
			} 	

			$(window).resize(function() {
				appendContent();
				pagin = 1;
				setIter();
			});

			function init () {
				if (!len) {
					$(_this).hide();
				}

				appendContent();
				$(_this).find('.len').text(len);
				$("a.gallery").lightbox();
			}

			function setSize () {
				var i_car_items = n_json.items;
				var setSizeItem = function (i) {
					if (i * ($(slides[0]).find(n_json.item_content).width() + n_json.padding ) > $(_this).width()  ){
					    i_car_items--;
					    setSizeItem(i_car_items);	
					}
				}
				setSizeItem(i_car_items);
				size = i_car_items;
			}
			

			function appendContent () {
				setSize ();

				
				checkNav();
				$(collection).empty();

				for (var i = 0; i < slides.length; i++) { 
					$(collection).append(slides[i]);
				}

				for (var i = cur_id; i < size; i++) {
					$(car_content).append( slides[i] );
				}
				
				$(car_content).find('.item').css('width', ( 100 / size) + "%" );

				c_size = size;
				
				

			}

			function checkNav () {
				
				if ( size >=len ) {
					$(nav_link).hide();
				} else {
					$(nav_link).show();
				}
			}

			function trigger (navigate) {
				if (block) {
					return false;
				}
				block = true;
				getWidth ();
				var w =  (size + 1) * sl_width;
				var s_w = 100/(size + 1);
				$(car_content).css({'width':w});
				if (navigate == "r") {
					pagin++;
					setIter();
					$(car_content).append( $(collection).find('.item').first() );
					$(car_content).find('.item').css('width',s_w+ "%");
					var an = $(_this).width()*1 - w;
					$(car_content).animate({'margin-left':an},400,function(){
						$(car_content).find('.item').first().appendTo(collection);
						$(car_content).removeAttr('style');
						$(_this).find('.item').css('width', ( 100 / size) + "%" );
						block = false;
					});
				} else {
					pagin--;
					setIter();
					$(car_content).prepend( $(collection).find('.item').last() );
					$(car_content).find('.item').css('width',s_w+ "%");
					var an = $(_this).width()*1 - w;
					$(car_content).css('margin-left',an);
					$(car_content).animate({'margin-left':0},400,function(){
						$(collection).prepend( $(car_content).find('.item').last() );
						$(car_content).removeAttr('style');
						$(_this).find('.item').css('width', ( 100 / size) + "%" );
						block = false;
					});
				}
					
			}

			
			function setIter() {
				pagin = (pagin>len) ? 1 : pagin;
				pagin = (pagin<1) ? len : pagin;  
				$(_this).find('.iter').text(pagin);
			}

			function getWidth () {
				sl_width = $(car_content).find('.item').first().width();
			}


			$(_this).on('click touchstart', 'a[class^=nav_a]', function (event) { 
				event.preventDefault();
				if ( $(event.currentTarget).attr('class').match(/right/gi) ) {
					 trigger('r');
				} else {
					 trigger('l');
				}	

			});

			init();
				
		});
	}
	
})(jQuery);