<?php echo $header; ?>

<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>

      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="icon-ok-sign"></i> <?php echo $success; ?></div>
      <?php } ?>
      <div class="row"><?php echo $column_left; ?>
        <div id="content" class="message"><?php echo $content_top; ?>
          <h2><?php echo $text_my_account; ?></h2>

            <div class="h_con f_con"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>
            <div class="h_con f_con"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>
            <div class="h_con f_con"><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></div>

          <h2><?php echo $text_my_orders; ?></h2>

            <div class="h_con f_con"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>
            <div class="h_con f_con"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></div>
            <?php if ($reward) { ?>
            <div class="h_con f_con"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></div>
            <?php } ?>
            <div class="h_con f_con"><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>
            <div class="h_con f_con"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></div>

          <h2><?php echo $text_my_newsletter; ?></h2>
          
            <div class="h_con f_con"><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php echo $footer; ?>