<?php echo $header; ?>
<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>
      <?php if ($error_warning) { ?>
      <div class="alert alert-error"><?php echo $error_warning; ?></div>
      <?php } ?>
        <div id="content" class="blocks"><?php echo $content_top; ?>
          <h1><?php echo $heading_title; ?></h1>

          <div class="res_block">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="con_form">
            <!-- <fieldset style="display: none;"> -->
                <label class="control-label" for="input-firstname"><?php echo $entry_firstname; ?> 
                <div class="border">
                  <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" />
                  <?php if ($error_firstname) { ?>
                  <div class="error"><?php echo $error_firstname; ?></div>
                  <?php } ?>
                </div>
                </label>

                <label class="control-label" for="input-email"><?php echo $entry_email; ?> 
                <div class="border">
                  <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" />
                  <?php if ($error_email) { ?>
                  <div class="error"><?php echo $error_email; ?></div>
                  <?php } ?>
                </div>
                </label>

                <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?> 
                <div class="border">
                  <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" />
                  <?php if ($error_telephone) { ?>
                  <div class="error"><?php echo $error_telephone; ?></div>
                  <?php } ?>
                </div>
                </label>
          <!-- </fieldset> -->
              <div class="pull-left" style="display: inline-block;"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
              <!-- <div class="pull-right"> -->
                <input type="submit" value="<?php echo $button_continue; ?>" class="button confirm" />
              <!-- </div> -->
            
          </form>
          </div>
        </div>
      <div class="clear"></div>
    </div>
  </div>
</main>
<?php echo $footer; ?>