<?php echo $header; ?>

<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>

      <?php if ($error_warning) { ?>
      <div class="alert alert-error"><?php echo $error_warning; ?></div>
      <?php } ?>
      <div class="row"><?php echo $column_left; ?>
        <div id="content" class="span9"><?php echo $content_top; ?>
          <h1><?php echo $heading_title; ?></h1>
          <p><?php echo $text_email; ?></p>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <fieldset>
              <legend><?php echo $text_your_email; ?></legend>
              <div class="control-group required">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <div class="controls">
                  <input type="email" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" />
                </div>
              </div>
            </fieldset>
            <div class="buttons clearfix">
              <div class="pull-left"><a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a></div>
              <div class="pull-right">
                <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
<?php echo $content_bottom; ?></div>
<?php echo $footer; ?>