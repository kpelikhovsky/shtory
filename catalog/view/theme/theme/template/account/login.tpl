<?php echo $header; ?>

<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>
    

      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="icon-ok-sign"></i> <?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-error"><?php echo $error_warning; ?></div>
      <?php } ?>
      <div class="row"><?php echo $column_left; ?>
        <div id="content" class="blocks"><?php echo $content_top; ?>
          <div class="res_block">
            <div class="span6">
              <div class="well">
                <h2><?php echo $text_new_customer; ?></h2>
                <p><strong><?php echo $text_register; ?></strong></p>
                <p><?php echo $text_register_account; ?></p>
                <a href="<?php echo $register; ?>" class="button btn-primary"><?php echo $button_continue; ?></a> </div>
            </div>
            <!-- <div class="span6"> -->
              <div class="res_block">
                <h2><?php echo $text_returning_customer; ?></h2>
                <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="con_form">
                  <!-- <div class="border"> -->
                    <label class="control-label" for="input-email"><?php echo $entry_email; ?>
                    <div class="border">
                      <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" />
                    </div>
                  </label>
                  <!-- </div> -->
                  <div class="control-group">
                    <label class="control-label" for="input-password"><?php echo $entry_password; ?>
                    <div class="border">
                      <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" />
                    </div>
                    </label>
                      <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a> 
                  </div>
                  <input type="submit" value="<?php echo $button_login; ?>" class="button btn-primary" />
                  <?php if ($redirect) { ?>
                  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                  <?php } ?>
                </form>
              </div>
            <!-- </div> -->
          </div>
          <div class="clear"></div>
    </div>

    
      <?php echo $column_right; ?></div>
  </div>
  <?php echo $content_bottom; ?></div>
</main>
<?php echo $footer; ?>