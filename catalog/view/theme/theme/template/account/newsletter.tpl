<?php echo $header; ?>

<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>

      <div class="row"><?php echo $column_left; ?>
        <div id="content" class="span9"><?php echo $content_top; ?>
          <h1><?php echo $heading_title; ?></h1>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="control-group">
                <div class="fc">
                  <label>
                    <input type="checkbox" name="newsletter" value="1" checked="checked" />
                    <?php echo $entry_newsletter; ?>
                  </label>
                  
                </div>
              </div>
            <div class="buttons clearfix">
              <div class="pull-left"><a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a></div>
              <div class="pull-right">
                <input type="submit" value="<?php echo $button_continue; ?>" class="button btn-primary" />
              </div>
            </div>
          </form>
        </div>  
      </div>
    </div>

    <?php echo $content_bottom; ?></div>
  <?php echo $column_right; ?></div>
  </div>
</main>
<?php echo $footer; ?>