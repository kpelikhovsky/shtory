<?php echo $header; ?>

<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>

      <?php if ($error_warning) { ?>
      <div class="alert alert-error"><?php echo $error_warning; ?></div>
      <?php } ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="blocks"><?php echo $column_left; ?>
        <div id="content" class="res_block">
          
          <p><?php echo $text_account_already; ?></p>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="con_form">

            <div class="control-group" style="display: <?php echo (count($customer_groups) > 1 ? 'display' : 'none'); ?>;">
                <div>
                  <?php foreach ($customer_groups as $customer_group) { ?>
                  <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                  <label class="radio fc">
                    <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                    Я <?php echo $customer_group['name']; ?> покупатель </label>
                  <?php } else { ?>
                  <label class="radio fc">
                    <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
                    Я <?php echo $customer_group['name']; ?> покупатель </label>
                  <?php } ?>
                  <?php } ?>
                </div>
              </div>
              
              <div class="control-group required">
                <label class="control-label" for="input-firstname">
                <div class="border">
                  <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?> *" id="input-firstname" />
                  <?php if ($error_firstname) { ?>
                  <div class="error"><?php echo $error_firstname; ?></div>
                  <?php } ?>
                </div>
                </label>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-lastname">
                <div class="border">
                  <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?> *" id="input-lastname" />
                  <?php if ($error_lastname) { ?>
                  <div class="error"><?php echo $error_lastname; ?></div>
                  <?php } ?>
                </div>
                </label>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-email">
                <div class="border">
                  <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?> *" id="input-email" />
                  <?php if ($error_email) { ?>
                  <div class="error"><?php echo $error_email; ?></div>
                  <?php } ?>
                </div>
                </label>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-telephone">
                <div class="border">
                  <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?> *" id="input-telephone" />
                  <?php if ($error_telephone) { ?>
                  <div class="error"><?php echo $error_telephone; ?></div>
                  <?php } ?>
                </div>
                </label>
              </div>

              <legend class="fc"><?php echo $text_your_address; ?></legend>
              <div class="control-group">
                <label class="control-label" for="input-company">
                <div class="border">
                  <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" />
                </div>
                </label>
              </div>
              
              <div class="control-group required">
                <div class="border">
                  <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?> *" id="input-address-1" />
                  <?php if ($error_address_1) { ?>
                  <div class="error"><?php echo $error_address_1; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input-address-2" for="input-address-2">
                </label>
                <div class="border">
                  <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" />
                </div>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-postcode"></label>
                <div class="border">
                  <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" />
                  <?php if ($error_postcode) { ?>
                  <div class="error"><?php echo $error_postcode; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-country"></label>
                <div class="border">
                  <select name="country_id" id="input-country">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($countries as $country) { ?>
                    <?php if ($country['country_id'] == $country_id) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  <?php if ($error_country) { ?>
                  <div class="error"><?php echo $error_country; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-zone"></label>
                <div class="border">
                  <select name="zone_id" id="input-zone">
                  </select>
                  <?php if ($error_zone) { ?>
                  <div class="error"><?php echo $error_zone; ?> *</div>
                  <?php } ?>
                </div>
              </div>

              <div class="control-group required">
                <label class="control-label" for="input-password"></label>
                <div class="border">
                  <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?> *" id="input-password" />
                  <?php if ($error_password) { ?>
                  <div class="error"><?php echo $error_password; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="control-group required">
                <label class="control-label" for="input-confirm"></label>
                <div class="border">
                  <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?> *" id="input-confirm" />
                  <?php if ($error_confirm) { ?>
                  <div class="error"><?php echo $error_confirm; ?></div>
                  <?php } ?>
                </div>
              </div>

              <div class="control-group">
                <div class="fc">
                  <label>
                    <input type="checkbox" name="newsletter" value="1" checked="checked" />
                    <?php echo $entry_newsletter; ?>
                  </label>
                  
                </div>
              </div>
            <?php if ($text_agree) { ?>
            <div class="buttons">
              <div class="pull-right">
                <?php echo $text_agree; ?>
                <?php if ($agree) { ?>
                <input type="checkbox" name="agree" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="agree" value="1" />
                <?php } ?>
                &nbsp; <input type="submit" value="<?php echo $button_continue; ?>" class="button confirm" />
              </div>
            </div>
            <?php } else { ?>
            <div class="buttons">
              <div class="pull-right">
                <input type="submit" value="<?php echo $button_continue; ?>" class="button confirm" />
              </div>
            </div>
            <?php } ?>
          </form>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  
    <div><?php echo $content_bottom; ?></div>
  </div>
</main>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').change(function() {
	var customer_group = [];

});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>

<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="icon-spinner icon-spin"></i>');
		},
		complete: function() {
			$('.icon-spinner').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#input-postcode').parent().parent().addClass('required');
			} else {
				$('#input-postcode').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone']) {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}

	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>