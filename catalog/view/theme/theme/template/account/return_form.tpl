<?php echo $header; ?>
<main class="content">
  <div class="box" id="main_box">
    <div class="grey_box">
      <ul class="in_b bread" >
        <?php $last_breadcrumb = array_pop($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
        <?php } ?>
        <li><span><?php echo $last_breadcrumb['text'];?></span></li>
      </ul>
      <?php if ($error_warning) { ?>
      <div class="alert alert-error"><?php echo $error_warning; ?></div>
      <?php } ?>
      <div class="row"><?php echo $column_left; ?>
        <div id="content" class="span9"> <?php echo $content_top; ?>
          <h1><?php echo $heading_title; ?></h1>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal con_form">
            <fieldset>
              <legend><?php echo $text_order; ?></legend>
              <div class="control-group required cf">
                <label class="first res_block">
                  <div class="border">
                    <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" />
                  </div>
                  <?php if ($error_firstname) { ?>
                  <div class="error"><?php echo $error_firstname; ?></div>
                  <?php } ?>
                </label>
              
                <label class="last res_block">
                <div class="border">
                  <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" />
                </div>                  
                  <?php if ($error_lastname) { ?>
                    <div class="error"><?php echo $error_lastname; ?></div>
                  <?php } ?>
                </label>
              </div>
              <div class="control-group required">
                <label class="first res_block">
                <div class="border">
                  <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" />
                </div>  
                  <?php if ($error_email) { ?>
                  <div class="error"><?php echo $error_email; ?></div>
                  <?php } ?>
                </label>
                <label class="last res_block">
                <div class="border">
                  <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" />
                </div>
                  <?php if ($error_telephone) { ?>
                  <div class="error"><?php echo $error_telephone; ?></div>
                  <?php } ?>
                </label>
              </div>
              <div class="control-group required cf">
                <label class="first res_block">
                <div class="border">
                  <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" />
                </div>  
                  <?php if ($error_order_id) { ?>
                  <div class="error"><?php echo $error_order_id; ?></div>
                  <?php } ?>
                </label>  
              </div>
            </fieldset>
            <fieldset>
              <legend><?php echo $text_product; ?></legend>
              <div class="control-group required cf">
                <label class="first res_block">
                <div class="border">
                  <input type="text" name="product" value="<?php echo $product; ?>" placeholder="<?php echo $entry_product; ?>" id="input-product" />
                </div>  
                  <?php if ($error_product) { ?>
                  <div class="error"><?php echo $error_product; ?></div>
                  <?php } ?>
                </label>
                <label class="last res_block" for="input-model">
                <div class="border">
                  <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" />
                </div>  
                  <?php if ($error_model) { ?>
                  <div class="error"><?php echo $error_model; ?></div>
                  <?php } ?>
                </label>
              </div>
              <div class="control-group cf">
                <label class="first res_block">
                <div class="border">
                  <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" />
                </div>
                </label>
              </div>
              <div class="control-group required cf">
                <div class="control-label"><?php echo $entry_reason; ?></div>
                <div class="controls">
                  <?php foreach ($return_reasons as $return_reason) { ?>
                  <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
                  <label class="radio">
                    <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" checked="checked" />
                    <?php echo $return_reason['name']; ?></label>
                  <?php } else { ?>
                  <label class="radio">
                    <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" />
                    <?php echo $return_reason['name']; ?></label>
                  <?php  } ?>
                  <?php  } ?>
                  <?php if ($error_reason) { ?>
                  <div class="error"><?php echo $error_reason; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="control-group required cf">
                <div class="control-label"><?php echo $entry_opened; ?></div>
                <div class="controls">
                  <label class="radio">
                    <?php if ($opened) { ?>
                    <input type="radio" name="opened" value="1" checked="checked" />
                    <?php } else { ?>
                    <input type="radio" name="opened" value="1" />
                    <?php } ?>
                    <?php echo $text_yes; ?></label>
                  <label class="radio">
                    <?php if (!$opened) { ?>
                    <input type="radio" name="opened" value="0" checked="checked" />
                    <?php } else { ?>
                    <input type="radio" name="opened" value="0" />
                    <?php } ?>
                    <?php echo $text_no; ?></label>
                </div>
              </div>
              <div class="control-group cf">
                <label class="border">
                  <textarea name="comment" rows="10" placeholder="<?php echo $entry_fault_detail; ?>" id="input-comment" class="input-xxlarge"><?php echo $comment; ?></textarea>
                </label>
              </div>
              <div class="control-group cf">
                <label class="first res_block">
                  <div class="border">
                    <input type="text" name="captcha" value="<?php echo $captcha; ?>" placeholder="<?php echo $entry_captcha; ?>" id="input-captcha" />
                  </div>  
                    <?php if ($error_captcha) { ?>
                    <div class="error"><?php echo $error_captcha; ?></div>
                    <?php } ?>
                </label>
                <label class="las res_block">
                  <div class="border">
                    <img src="index.php?route=account/return/captcha" alt="" />
                  </div>  
                </label>
              </div>
            </fieldset>
            <?php if ($text_agree) { ?>
            <div class="buttons clearfix">
              <div class="pull-left"><a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a></div>
              <div class="pull-right"><?php echo $text_agree; ?>
                <?php if ($agree) { ?>
                <input type="checkbox" name="agree" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="agree" value="1" />
                <?php } ?>
                <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
              </div>
            </div>
            <?php } else { ?>
            <div class="buttons cf">
              <div class="fl-left"><a href="<?php echo $back; ?>" class="button fsb"><?php echo $button_back; ?></a></div>
              <div class="fl-right">
                <input type="submit" value="<?php echo $button_continue; ?>" class="button fsb" />
              </div>
            </div>
            <?php } ?>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>  
    <?php echo $content_bottom; ?></div>
  <?php echo $column_right; ?></div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script>
<?php echo $footer; ?>