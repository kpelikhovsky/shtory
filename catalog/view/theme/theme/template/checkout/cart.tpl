<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<div style="position:relative">
				<h1><?php echo $heading_title; ?></h1>
				<a class="continue pab" href="<?php echo $continue; ?>"><i class="bg left"> </i><?php echo $button_shopping; ?></a>
			<div>
			<div class="cart">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="cart_form">
					<div class="cart_title">
						<div class="cart_title_content in_b">
							<div class="name"><?php echo $column_name; ?></div>
							<div class="size ta_c"><?php echo $column_quantity; ?></div>
							<div class="price ta_c"><?php echo $column_price; ?></div>
							<div class="summ ta_c"><?php echo $column_total; ?></div>
						</div>
					</div>
					<?php foreach ($products as $product) { ?>
						<div class="cart_container">
							<div class="cart_content in_b">
								<div class="name">
									<div class="in_b">
										<?php if ($product['thumb']) { ?>
										<div class="imgBg va_m">
											<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
										</div>
										<?php } ?>
										<div class="title va_m"><?php echo $product['name']; ?></div>
									</div>
								</div>
								<div class="un_cart">
									<div class="in_b">
										<div class="size ta_c">
											<span class="t_text">кол-во:</span>
											<div class="quan">
												<input class="submit_onchange" name="quantity[<?php echo $product['key']; ?>]" type="text" min="1" value="<?php echo $product['quantity']; ?>" size="1" autocomplete="off">
												<a href="#" class="tp"></a>
												<a href="#" class="bt"></a>
											</div>
										</div>
										<div class="price ta_c">
											<span class="t_text">цена:</span>
											<div class="in_b">
												<?php echo $product['price']; ?>
											</div>
										</div>
										
										<div class="summ ta_c">
											<span class="t_text">сумма:</span>
											<div class="in_b">
												<?php echo $product['total']; ?>
												<a href="<?php echo $product['remove']; ?>" class="remove" data-toggle="tooltip" title="<?php echo $button_remove; ?>"></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					
					<?php foreach ($totals as $total) { ?>
						<?php if ($total['code'] == 'total') { ?>
						<div class="itog">
							<span class="itog_text"><?php echo $total['title']; ?>:</span>
							<?php echo $total['text']; ?>
						</div>
						<?php } ?>
					<?php } ?>
					
					<div class="of_z">
						<a href="<?php echo $checkout; ?>" class="button"><?php echo $button_checkout; ?></a>
						<a href="<?php echo $continue; ?>" class="continue"><i class="bg left"> </i><?php echo $button_shopping; ?></a>
					</div>
				</form>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</main><!-- .content -->
<script type="text/javascript"><!--
	timer = 0;
	$('.submit_onchange').change(function() {
		window.clearTimeout(timer);
		timer = window.setTimeout(function() {
			$('#cart_form').submit();
		}, 1000);
	});
--></script>
<?php echo $footer; ?>