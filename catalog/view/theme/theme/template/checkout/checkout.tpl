<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
			<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<h1><?php echo $heading_title; ?></h1>
			
			<div class="clear"></div>

			<div class="blocks" id="#accordion">
				<h2 class="h_con f_con">
					<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="spbg bg_new"></i></a>
				</h2>
				<div id="collapse-checkout-option" class="accordion-body collapse">
					<div class="accordion-inner"></div>
				</div>
				<div class="clear"></div>
				<?php if (!$logged) { ?>
					<h2 class="h_con f_con"><a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="icon-caret-down"></i></a></h2>
					<div id="collapse-payment-address" class="accordion-body collapse" hidden="hidden">
						<div class="accordion-inner"></div>
					</div> 
				<?php } else { ?>
					<!-- <h2 class="h_con f_con"><a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="icon-caret-down"></i></a></h2>
					<div id="collapse-payment-address" class="accordion-body collapse" hidden>
						<div class="accordion-inner"></div>
					</div> -->
				<?php } ?>
				<div class="clear"></div>
				<?php if ($shipping_required) { ?>
					<!-- <h2 class="h_con f_con"><a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address.$text_checkout_payment_method; ?> <i class="icon-caret-down"></i></a></h2> -->
					<div>
						<div class="res_block" style="float: left;" hidden="hidden">
							<div id="collapse-shipping-address" class="accordion-body collapse">
								<div class="accordion-inner"></div>
							</div>
						</div>
						<div class="res_block" style="float: left;" hidden="hidden">
							<div id="collapse-shipping-method" class="accordion-body collapse">
								<div class="accordion-inner"></div>
							</div>
						</div>
						<!--<div class="res_block" style="float: left;" hidden="hidden">
							 <div id="collapse-payment-address" class="accordion-body collapse">
								<div class="accordion-inner"></div>
							</div> 
						</div>-->
						<div class="res_block" style="float: left;">
							<div id="collapse-payment-method" class="accordion-body collapse">
								<div class="accordion-inner"></div>
							</div>
						</div>
					</div>
					<div class="clear"></div>

					<!-- <h2 class="h_con f_con"><a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="icon-caret-down"></i></a></h2>
					<div class="clear"></div> -->
				<?php } ?>

				<!-- <h2 class="h_con f_con"><a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="icon-caret-down"></i></a></h2>
				<div class="res_block">
					<div id="collapse-payment-method" class="accordion-body collapse">
						<div class="accordion-inner"></div>
					</div>
				</div>
				<div class="clear"></div> -->
				
				<h2 class="h_con f_con"><a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_confirm; ?> <i class="icon-caret-down"></i></a></h2>
				<div class="res_block">
					<div id="collapse-checkout-confirm" class="accordion-body collapse">
						<div class="accordion-inner"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>

		</div>
	</div>
</main>
<br />
<script type="text/javascript"><!--
	$(document).delegate('.accordion-toggle', 'click', function() 
	{
		$('.accordion-body').slideUp('fast');
	
		$(this).parent().parent().find($(this).attr('href')).slideDown('fast');

		if($(this).attr('href') == '#collapse-shipping-address')
		{
			$('#collapse-shipping-method').slideDown('fast');
			$('#collapse-payment-method').slideDown('fast');
		}
		return false;
	});
	
	$(document).on('change', 'input[name=\'account\']', function() {
		if (this.value == 'register') {
			$('#collapse-payment-address').parent().find('.accordion-heading a').html('<?php echo $text_checkout_account; ?> <i class="icon-caret-down"></i>');
		} else {
			$('#collapse-payment-address').parent().find('.accordion-heading a').html('<?php echo $text_checkout_payment_address; ?> <i class="icon-caret-down"></i>');
		}
	});

	<?php if (!$logged) { ?>
		$(document).ready(function() {
			$.ajax({
				url: 'index.php?route=checkout/login',
				dataType: 'html',
				success: function(html) {
					$('#collapse-checkout-option .accordion-inner').html(html);

					$('#collapse-checkout-option').slideDown();
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	<?php } else { ?>
		$(document).ready(function() {
			$.ajax({
				url: 'index.php?route=checkout/shipping_address',
				dataType: 'html',
				success: function(html) {
					$('#collapse-shipping-address .accordion-inner').html(html);

					$('#collapse-shipping-address').slideDown();

					
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});

			$.ajax({
				url: 'index.php?route=checkout/shipping_method',
				dataType: 'html',
				success: function(html) {
					$('#collapse-shipping-method .accordion-inner').html(html);

					$('#collapse-shipping-method').slideDown();

					
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});

			$.ajax({
				url: 'index.php?route=checkout/payment_address',
				dataType: 'html',
				success: function(html) {
					$('#collapse-payment-address .accordion-inner').html(html);

					// $('#collapse-payment-address').slideDown();
					
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});

			$.ajax({
				url: 'index.php?route=checkout/payment_method',
				dataType: 'html',
				success: function(html) {
					$('#button-shipping-address').trigger('click');
					// $('#button-shipping-method').trigger('click');
					$('#button-payment-address').trigger('click');
					// $('button-payment-method').click();
					
					$('#collapse-payment-method .accordion-inner').html(html);

					$('#collapse-payment-method').slideDown();

					// $('#collapse-payment-address').slideUp();
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
			
		});
	<?php } ?>

	// Checkout
	$(document).delegate('#button-account', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').val(),
			dataType: 'html',
			beforeSend: function() {
				$('#button-account').button('loading');
			},
			complete: function() {
				$('#button-account').button('reset');
			},
			success: function(html) {
				$('.alert').remove();

				$('#collapse-payment-address .accordion-inner').html(html);

				$('#collapse-payment-address').slideDown();

				$('#collapse-checkout-option').slideUp();

				// $('.checkout-heading a').remove();

				// $('#checkout .checkout-heading').append('');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Login
	$(document).delegate('#button-login', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/login/validate',
			type: 'post',
			data: $('#collapse-checkout-option #login :input'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-login').button('loading');
			},
			complete: function() {
				$('#button-login').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#collapse-checkout-option .accordion-inner').prepend('<div class="alert alert-error"><i class="icon-exclamation-sign"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Register
	$(document).delegate('#button-register', 'click', function(event) {
		// event.stopPropagation();
		$('#button-register').unbind('click');
		$.ajax({
			url: 'index.php?route=checkout/register/validate',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-register').button('loading');
			},
			complete: function() {
				$('#button-register').button('reset');
			},
			success: function(json) 
			{
				$('.alert, .text-error').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-address .accordion-inner').prepend('<div class="alert alert-error"><i class="icon-exclamation-sign"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						$('#input-payment-' + i.replace('_', '-')).after('<span class="text-error"><br />' + json['error'][i] + '</span>');
					}
				} else {
					<?php if ($shipping_required) { ?>
						var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

						if (shipping_address) {
							$.ajax({
								url: 'index.php?route=checkout/shipping_method',
								dataType: 'html',
								success: function(html) {
									$('#collapse-shipping-method .accordion-inner').html(html);

									// $('#collapse-shipping-method').slideDown();

									// $('#collapse-payment-address').slideUp();

									$('#checkout .checkout-heading a').remove();
									$('#payment-address .checkout-heading a').remove();
									$('#shipping-address .checkout-heading a').remove();
									$('#shipping-method .checkout-heading a').remove();
									$('#payment-method .checkout-heading a').remove();

									$('#shipping-address .checkout-heading').append('');
									$('#payment-address .checkout-heading').append('');

									$.ajax({
										url: 'index.php?route=checkout/shipping_address',
										dataType: 'html',
										success: function(html) {
											$('#collapse-shipping-address .accordion-inner').html(html);
										},
										error: function(xhr, ajaxOptions, thrownError) {
											alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
										}
									});
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						} else {
							$.ajax({
								url: 'index.php?route=checkout/shipping_address',
								dataType: 'html',
								success: function(html) {
									$('#collapse-shipping-address .accordion-inner').html(html);

									// $('#collapse-shipping-address').slideDown();

									// $('#collapse-payment-address').slideUp();

									$('#checkout .checkout-heading a').remove();
									$('#payment-address .checkout-heading a').remove();
									$('#shipping-address .checkout-heading a').remove();
									$('#shipping-method .checkout-heading a').remove();
									$('#payment-method .checkout-heading a').remove();

									$('#payment-address .checkout-heading').append('');
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						}
					<?php } else { ?>
						$.ajax({
							url: 'index.php?route=checkout/payment_method',
							dataType: 'html',
							success: function(html) {
								$('#collapse-payment-method .accordion-inner').html(html);

								// $('#collapse-payment-method').slideDown();

								// $('#collapse-payment-address').slideUp();

								$('#button-shipping-address').trigger('click');
								
								$('#button-payment-address').trigger('click');

								$('#checkout .checkout-heading a').remove();
								$('#payment-address .checkout-heading a').remove();
								$('#payment-method .checkout-heading a').remove();

								$('#payment-address .checkout-heading').append('');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					<?php } ?>

					$.ajax({
						url: 'index.php?route=checkout/payment_address',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-address .accordion-inner').html(html);

							$('#payment-address .checkout-heading span').html('<?php echo $text_checkout_payment_address; ?>');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Payment Address
	$(document).delegate('#button-payment-address', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/payment_address/validate',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-payment-address').button('loading');
			},
			complete: function() {
				$('#button-payment-address').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-address .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
								$('#input-payment-' + i.replace('_', '-')).after('<span class="text-error"><br />' + json['error'][i] + '</span>');
					}
				} else {
					<?php if ($shipping_required) { ?>
					$.ajax({
						url: 'index.php?route=checkout/shipping_address',
						dataType: 'html',
						success: function(html) {
							$('#collapse-shipping-address .accordion-inner').html(html);

							// $('#collapse-shipping-address').slideDown();

							$('#collapse-payment-address').slideUp();

							$('#collapse-payment-address .checkout-heading a').remove();
							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#collapse-payment-address').append('');	
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
					<?php } else { ?>
					$.ajax({
						url: 'index.php?route=checkout/payment_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-method .accordion-inner').html(html);

							$('#collapse-payment-method').slideDown();

							// $('#collapse-payment-address').slideUp();

							$('#payment-address .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#payment-address .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
					<?php } ?>

					$.ajax({
						url: 'index.php?route=checkout/payment_address',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-address .accordion-inner').html(html);
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Shipping Address
	$(document).delegate('#button-shipping-address', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/shipping_address/validate',
			type: 'post',
			data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address select'),
			dataType: 'json',
			beforeSend: function() {
			$('#button-shipping-address').button('loading');
			},
			complete: function() {
			$('#button-shipping-address').button('reset');
			},
			success: function(json) {
				$('.alert-warning, .alert-error').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-shipping-address .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

			for (i in json['error']) {
						$('#input-shipping-' + i.replace('_', '-')).after('<span class="text-error"><br />' + json['error'][i] + '</span>');
			}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/shipping_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-shipping-method .accordion-inner').html(html);

							// $('#collapse-shipping-method').slideDown();

				$('#collapse-shipping-address').slideUp();

							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#shipping-address .checkout-heading').append('');

							$.ajax({
								url: 'index.php?route=checkout/shipping_address',
								dataType: 'html',
								success: function(html) {
									$('#collapse-shipping-address .accordion-inner').html(html);
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});

					$.ajax({
						url: 'index.php?route=checkout/payment_address',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-address .accordion-inner').html(html);
							$('#collapse-payment-address').slideUp();
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Guest
	$(document).delegate('#button-guest', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/guest/validate',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
			 	$('#button-guest').button('loading');
			},
			complete: function() {
			$('#button-guest').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-address .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

			for (i in json['error']) {
						$('#input-payment-' + i.replace('_', '-')).after('<span class="text-error"><br />' + json['error'][i] + '</span>');
			}
				} else {
					<?php if ($shipping_required) { ?>
					var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

					if (shipping_address) {
						$.ajax({
							url: 'index.php?route=checkout/shipping_method',
							dataType: 'html',
							success: function(html) {
								$('#collapse-shipping-method .accordion-inner').html(html);

								$('#collapse-shipping-method').slideDown();

					$('#collapse-payment-address').slideUp();

								$('#payment-address .checkout-heading a').remove();
								$('#shipping-address .checkout-heading a').remove();
								$('#shipping-method .checkout-heading a').remove();
								$('#payment-method .checkout-heading a').remove();

								$('#payment-address .checkout-heading').append('');
								$('#shipping-address .checkout-heading').append('');

								$.ajax({
									url: 'index.php?route=checkout/guest_shipping',
									dataType: 'html',
									success: function(html) {
										$('#collapse-shipping-address .accordion-inner').html(html);
									},
									error: function(xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									}
								});
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					} else {
						$.ajax({
							url: 'index.php?route=checkout/guest_shipping',
							dataType: 'html',
							success: function(html) {
								$('#collapse-shipping-address .accordion-inner').html(html);

					$('#collapse-shipping-address').slideDown();

					$('#collapse-payment-address').slideUp();

								$('#payment-address .checkout-heading a').remove();
								$('#shipping-address .checkout-heading a').remove();
								$('#shipping-method .checkout-heading a').remove();
								$('#payment-method .checkout-heading a').remove();

								$('#payment-address .checkout-heading').append('');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					}
					<?php } else { ?>
					$.ajax({
						url: 'index.php?route=checkout/payment_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-method .accordion-inner').html(html);

				$('#collapse-payment-method').slideDown();

				$('#collapse-payment-address').slideUp();

							$('#payment-address .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#payment-address .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
					<?php } ?>
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Guest Shipping
	$(document).delegate('#button-guest-shipping', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/guest_shipping/validate',
			type: 'post',
			data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-guest-shipping').button('loading');
		},
			complete: function() {
			$('#button-guest-shipping').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-shipping-address .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

			for (i in json['error']) {
						$('#input-shipping-' + i.replace('_', '-')).after('<span class="text-error"><br />' + json['error'][i] + '</span>');
			}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/shipping_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-shipping-method .accordion-inner').html(html);

							$('#collapse-shipping-method').slideDown();

				$('#collapse-shipping-address').slideUp();

							$('#shipping-address .checkout-heading a').remove();
							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#shipping-address .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$(document).delegate('#button-shipping-method', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/shipping_method/validate',
			type: 'post',
			data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-shipping-method').button('loading');
		},
			complete: function() {
			$('#button-shipping-method').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-shipping-method .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/payment_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-method .accordion-inner').html(html);

				// $('#collapse-payment-method').slideDown();

				$('#collapse-shipping-method').slideUp();

							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#shipping-method .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});


	// Click buttons for hidden fields
	function sendAll()
	{
		$.ajax({
			url: 'index.php?route=checkout/shipping_method/validate',
			type: 'post',
			data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-shipping-method').button('loading');
		},
			complete: function() {
			$('#button-shipping-method').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-shipping-method .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/payment_method',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-method .accordion-inner').html(html);

							// $('#collapse-payment-method').slideDown();

							$('#collapse-shipping-method').slideUp();

							$('#shipping-method .checkout-heading a').remove();
							$('#payment-method .checkout-heading a').remove();

							$('#shipping-method .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}

	$(document).delegate('#button-payment-method', 'click', function() 
	{
		sendAll();
		$.ajax({
			url: 'index.php?route=checkout/payment_method/validate',
			type: 'post',
			data: $('#collapse-payment-method select, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
			dataType: 'json',
			beforeSend: function() {
			 	$('#button-payment-method').button('loading');
		},
			complete: function() {
				$('#button-payment-method').button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-method .accordion-inner').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/confirm',
						dataType: 'html',
						success: function(html) {
							$('#collapse-checkout-confirm .accordion-inner').html(html);

				$('#collapse-checkout-confirm').slideDown();

				$('#collapse-payment-method').slideUp();

							$('#payment-method .checkout-heading a').remove();

							$('#payment-method .checkout-heading').append('');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
//--></script>
<?php echo $footer; ?>