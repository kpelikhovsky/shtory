<div class="res_block" style="margin-right: 6%;">
	<h2 class="h_con f_con">Я новый покупатель <i class="spbg bg_new"></i> </h2>
	<div class="con_form">
		<label>
			<div class="border">
			<?php if ($account == 'register') { ?>
				<input type="radio" name="account" value="register" checked="checked" />
			<?php } else { ?>
				<input type="radio" name="account" value="register" />
			<?php } ?>
			<?php echo $text_register; ?>
			</div>
		</label>
		
		<?php if ($guest_checkout) { ?>
		<label>
			<div class="border">
			<?php if ($account == 'guest') { ?>
				<input type="radio" name="account" value="guest" checked="checked" />
			<?php } else { ?>
				<input type="radio" name="account" value="guest" />
			<?php } ?>
			</div>
		</label>
		<?php } ?>

		<input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="button fsb" />
	</div>
</div>
<div class="res_block" id="login">
	<h2 class="h_con f_con">Вход <i class="spbg bg_enter"></i> </h2>
	<div class="con_form">
		<label>
			<div class="border">
				<input type="text" name="email" placeholder="E-mail" id="input-email" />
			</div>
		</label>

		<label>
			<div class="border">
				<input type="password" name="password" placeholder="Пароль" id="input-password" />
			</div>
		</label>

		<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br />
		<br />
		
		<input type="button" class="button fsb" value="Вход" id="button-login" />
	</div>
</div>
