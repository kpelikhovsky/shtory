<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="icon-exclamation-sign"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<div class="row-fluid">
  <div class="con_form">
    <p><?php echo $text_payment_method; ?></p>
    <label>
      <div class="border">
        <select name="payment_method">
          <?php foreach ($payment_methods as $payment_method) { ?>
          <?php if ($payment_method['code'] == $code || !$code) { ?>
          <option value="<?php echo $payment_method['code']; ?>" selected="selected"><?php echo $payment_method['title']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
    </label>
  </div>
    
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="pull-right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        &nbsp; <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
      </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="pull-right">
        <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button fsb" />
      </div>
    </div>
    <?php } ?>
  </div>
<script type="text/javascript"><!--
$(document).ready(function() {
	/*
    $('.colorbox').colorbox({
        maxWidth: 640,
        width: "85%",
        height: 480
    });
	*/
});
//--></script>