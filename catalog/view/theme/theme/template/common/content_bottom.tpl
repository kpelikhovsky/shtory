<?php if (sizeof($products) > 0): ?>
<h2 class="car_h">Распродажа</h2>
<div class="carusel carus">
	<div class="car_container">
		<div class="hide_items">
			<?php foreach ($products as $item) { ?>
			<div class="item">
				<div class="sm_content2">
					<img src="<?= $item['thumb'];?>">
					<div class="price">
						<span>
							<span class="new_price"><?= $item['special']?></span><br/>
							<span class="old_price"><?= $item['price']?></span>
						</span>
					</div>
					<div class="item_hover">
						<a href="<?= $item['href']?>"><span class="buy_text"><b><?= $item['name']?></b><br/><?= substr($item['description'], 0, 50)."..."; ?></span></a><br/>
						<a href="<?= $item['href']?>" class="buy"><span class="bg"></span><span>Купить</span></a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="car_content">
		</div>
	</div>
	<div class="pagination">

		<a href="#" class="nav_a left_nav"><span class="bg"></span></a>
		<span class="iter">1</span><span>/</span ><span class="len">4</span>
			<a href="#" class="nav_a right_nav"><span class="bg"></span></a>
	</div>
	<a href="#" class="nav_a sl_left_nav"><span class="bg"></span></a>
		<a href="#" class="nav_a sl_right_nav"><span class="bg"></span></a>
</div>
<?php endif; ?>