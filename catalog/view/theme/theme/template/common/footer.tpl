</div><!-- .wrapper -->

<footer class="footer">
	<div class="box">
		<div class="subscr">
			<span>Подпишитесь на рассылку новостей:</span>
			<div>
				<label><input type="text" value="" placeholder="Введите ваш e-mail" /></label>
				<a href="<?php echo $newsletter; ?>"><input type="button" value=""/></a>
			</div>
		</div>
		<?php if ($informations) { ?>
		<div class="list">
			<h3><?php echo $text_information; ?></h3>
			<ul>
				<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<?php } ?>

		<div class="list">
			<h3><?php echo $text_service; ?></h3>
			<ul>
				<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
				<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
				<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
			</ul>
		</div>

		<div class="list">
			<h3><?php echo $text_account; ?></h3>
			<ul>
				<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
				<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
				<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
				<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
			</ul>
		</div>

		<div class="list_soc">
			<ul>
				<li><a href="http://vk.com/shtorykiseya" id="vk"></a></li>
				<li><a href="#" id="tw"></a></li>
				<li><a href="#" id="fb"></a></li>
				<li><a href="#" id="g"></a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="author">
			<span><?php echo date('Y'); ?> &copy; shtorykiseya.com</span><br/>
			<!-- <a href="http://wowagency.com.ua">Создание сайта</a> -->
		</div>	

	</div>
</footer><!-- .footer -->
<div class="popup telPopup resize">
	<div class="popupContainer">
		<div class="popupContent">
			<a class="popupExit" href="#" title="exit"></a>
			<div class="popuptelPContent">
				<span class="f_name">Александр:</span>
				<span class="number">
					<span class="f_num">093</span>
					<span class="n_num">728 28 29</span>
				</span>
				<span class="number">
					<span class="f_num">095</span>
					<span class="n_num">633 35 53</span>
				</span>
				<span class="number">
					<span class="f_num">097</span>
					<span class="n_num">912 52 52</span>
				</span>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<div class="popup logPopup resize">

	<div class="popupContainer">
		<div class="popupContent">
			<a class="popupExit" href="#" title="exit"></a>
			<div class="popuploginPContent">
				<?php echo $login; ?>
			</div>
		</div>
	</div>
</div>

<!-- Start SiteHeart code -->
<script>
(function(){
var widget_id = 696475;
_shcp =[{widget_id : widget_id}];
var lang =(navigator.language || navigator.systemLanguage 
|| navigator.userLanguage ||"ru")
.substr(0,2).toLowerCase();
var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
var hcc = document.createElement("script");
hcc.type ="text/javascript";
hcc.async =true;
hcc.src =("https:"== document.location.protocol ?"https":"http")
+"://"+ url;
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<!-- End SiteHeart code -->

</body>
</html>