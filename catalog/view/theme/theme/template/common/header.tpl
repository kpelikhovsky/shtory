<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
	<meta charset="UTF-8" />
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />

	<?php if ($description) { ?><meta name="description" content="<?php echo $description; ?>" /><?php } ?>
	<?php if ($keywords) { ?><meta name="keywords" content= "<?php echo $keywords; ?>" /><?php } ?>
	<?php if ($icon) { ?><link href="<?php echo $icon; ?>" rel="icon" /><?php } ?>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="catalog/view/javascript/lightbox/jquery.lightbox.min.js"></script>
	<script type="text/javascript" src="catalog/view/javascript/common.js"></script> 
	<script type="text/javascript" src="catalog/view/javascript/plugins.js"></script>	
	<script type="text/javascript" src="catalog/view/javascript/main.js"></script> 
	<link href="catalog/view/javascript/lightbox/jquery.lightbox.min.css" type="text/css" rel="stylesheet">
	<link href="catalog/view/theme/theme/stylesheet/style.css" type="text/css" rel="stylesheet">

	<?php foreach ($links as $link) { ?><link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" /><?php } ?>
	<?php foreach ($styles as $style) { ?><link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" /><?php } ?>

	<?php foreach ($scripts as $script) { ?><script src="<?php echo $script; ?>" type="text/javascript"></script><?php } ?>
	<?php echo $google_analytics; ?>
</head>
<body>
<?php function pluralForm($n, $form1, $form2, $form5) {
	$n = abs($n) % 100;
	$n1 = $n % 10;
	if ($n > 10 && $n < 20) return $form5;
	if ($n1 > 1 && $n1 < 5) return $form2;
	if ($n1 == 1) return $form1;
	return $form5;
} ?>
<div id="cbg"></div>	
<div class="wrapper">
	<header class="header">
		<div class="box p_r first_box resize">
			<div class="inion">
				<div id="tel">
					<span class="bg"></span>
					<span class="number">
						<span class="f_num">093</span>
						<span class="n_num">728 28 29</span>
					</span>
					<span class="select popupHref" data-popup=".telPopup">
						<span class="bg center"></span>
					</span>
				</div>
				<div id="logo">
					<a href="#"><img src="catalog/view/theme/theme/image/logo.png" alt=""/></a>
				</div>
			</div>
			<div class="search">
				<label>
					<input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
				</label>
				<input type="submit" value="" class="button-search" />
			</div>
			<div class="rec_block">
				<?php if (!$this->customer->isLogged()) { ?><a href="#" class="button popupHref" data-popup=".logPopup">Вход</a>
				<?php } else { ?> <a href="<?php echo $this->url->link('account/logout', '', 'SSL'); ?>" class="button" data-popup=".logPopup">Выход</a> <?php } ?>
				<div class="clear"></div>
				<div class="rec">
					<span class="bg"></span>
					<a href="<?php echo $shopping_cart ?>" class="title">Корзина</a><br/>
					<a href="<?php echo $shopping_cart ?>" class="s_title"><span class="size"><?php echo $item_count; ?></span> товаров</a>
				</div>
				<a href="#" class="hide_header">
					<span class="bg center active"></span>
				</a>
				<a href="#" class="hide_menu">
					<span></span>
					<span></span>
					<span></span>
				</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="box p_r second_box resize">
				<div class="rec">
					<a href="<?php echo $shopping_cart ?>" class="title">Корзина</a>
					<a href="<?php echo $shopping_cart ?>" class="s_title"><span class="size"><?php echo $item_count; ?></span> товаров</a>
				</div>

				<a href="#" class="hide_header">
					<span class="bg center"></span>
				</a>
				<a href="#" class="hide_menu">
					<span></span>
					<span></span>
					<span></span>
				</a>
			<div class="clear"></div>
		</div>
	</header><!-- .header-->
	
	<div class="menu top_menu resize">
			<div class="box">
			<ul>
				<li class="active"><a href="#">О нас</a></li>
				<li><a href="#">Каталог <span class="select"><span class="bg center"></span></span></a>
					<?php if ($categories) { ?>
					<ul class="resize">
						<?php foreach ($categories as $category) { ?>
							<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
				</li>
				<li><a href="<?php echo $special ?>">Акции</a></li>
				<li><a href="<?php echo $news ?>">Новости</a></li>
				<li><a href="<?php echo $delivery ?>">Оплата и доставка</a></li>
				<li><a href="<?php echo $contact ?>">Контакты</a></li>
			</ul>
		</div>
	</div>
<div id="notification">
	<?php if ($attention) { ?>
		<div class="alert alert-info"><?php echo $attention; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>

	<?php if ($success) { ?>
		<div class="alert alert-success"><i class="icon-ok-sign"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>

	<?php if ($error_warning) { ?>
		<div class="alert alert-error"><?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
</div>