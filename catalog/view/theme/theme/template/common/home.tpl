<?php echo $header; ?>
	<main class="content">
		<div class="box" id="main_box">
			<?php echo $content_top; ?>
            <div class="text_blocks">
           		<div class="text_block">
           			<a href="<?php echo $categories[0]['href']; ?>">
	           			<img src="<?php echo $categories[0]['thumb']; ?>"  class="img_bg" alt=""/>
	           			<div class="f_title">
	           				<a href="<?php echo $categories[0]['href']; ?>" class="f_title_text ber">Шторы-нити</a><br/>
	           				<span class="f_text">безграничные дизайнерские решения</span><br/>
	           				<a href="<?php echo $categories[0]['href']; ?>" class="look_cat">Смотреть каталог</a>
	           			</div>
           			</a>
           		</div>
           		<div class="text_block">
           			<a href="<?php echo $categories[1]['href']; ?>">
	           			<img src="<?php echo $categories[1]['thumb']; ?>"  class="img_bg" alt=""/>
	           			<div class="f_title">
	           				<a href="<?php echo $categories[1]['href']; ?>" class="f_title_text ber">Фотошторы</a><br/>
	           				<span class="f_text">безграничные дизайнерские решения</span><br/>
	           				<a href="<?php echo $categories[1]['href']; ?>" class="look_cat">Смотреть каталог</a>
	           			</div>
           			</a>
           		</div>

           		<?php foreach ($news_data as $news) { ?>
					<div class="text_block text_content">
						
						<div class="div_center">

							<?php if ($news['thumb']) { ?>
								<div class="img_h">
									
									<img src="<?php echo $news['thumb']; ?>" title="<?php echo $news['title']; ?>" alt="<?php echo $news['title']; ?>" id="image" />
									
								</div>
							<?php } ?>
							<h3><?php echo $news['title']; ?></h3>
							<span class="f_date">
								<b>Добавлена: </b>
								<?php echo $news['posted']; ?>
							</span>
							<div class="fs14">
								<p>
									<?php echo $news['description']; ?> <br />
								</p>
								<div class="a_center">
		           					<a href="<?php echo $news['href']; ?>" class="fs14 button tt_l" >Подробнее</a>
		           				</div>
							</div>
						</div>
					</div>
			
			<?php } ?>
            </div>
            	
		</div>
	</main><!-- .content -->

<?php echo $footer; ?>