<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			
			<h1><?php echo $heading_title; ?></h1>
			
			<div class="blocks">
				<div class="res_block">
					<h2 class="h_con">Адрес <i class="spbg bg_addr"></i> </h2>
					<div class="blocks_text">
						<?php echo $contacts_address; ?>
						<br/><br/>
						
						<a id="firmsonmap_biglink" href="http://maps.2gis.ru/#/?history=project/odessa/center/30.638490835621,46.442741164166/zoom/18/state/widget/id/1970853117973166/firms/1970853117973166">Перейти к большой карте</a>
						<script charset="utf-8" type="text/javascript" src="http://firmsonmap.api.2gis.ru/js/DGWidgetLoader.js"></script>
						<script charset="utf-8" type="text/javascript">new DGWidgetLoader({"borderColor":"#a3a3a3","width":"100%","height":"397","wid":"5a1356f9c8c26c956a9a50add73eed73","pos":{"lon":"30.638490835621","lat":"46.442741164166","zoom":"18"},"opt":{"ref":"hidden","card":["name","contacts","schedule","payings"],"city":"odessa"},"org":[{"id":"1970853117973166"}]});</script>
						<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
					</div>
				</div>

				<div class="res_block">
					<h2 class="h_con">Контактные данные <i class="spbg bg_cont"></i> </h2>
					<div class="blocks_text">
						<?php echo $contacts_info; ?>
					</div>

					<h2 class="h_con">Реквизиты <i class="spbg bg_recv"></i></h2>
					<div class="blocks_text">
						<b>
							<?php echo $contacts_requisits; ?>
							
							<a href="http://vk.com/shtorykiseya">Группа в контакте</a>
						</b>
					</div>	
				</div>
				<div class="clear"></div>
			</div>

			<form class="con_form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<h2 class="h_con f_con">ФОРМА ОБРАТНОЙ СВЯЗИ<i></i> </h2>
				<label class="first res_block">
					<div class="border">
						<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" />
						<?php if ($error_name) { ?>
							<div class="alert alert-error"><?php echo $error_name; ?></div>
						<?php } ?>
					</div>
				</label>
				<label class="last res_block">
					<div class="border">
						<input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>">
						<?php if ($error_email) { ?>
							<div class="alert alert-error"><?php echo $error_email; ?></div>
						<?php } ?>
					</div>
				</label>
				<div class="clear"></div>
				<label class="border">
					<textarea name="enquiry" cols="40" rows="10" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
					<?php if ($error_enquiry) { ?>
						<div class="alert alert-error"><?php echo $error_enquiry; ?></div>
					<?php } ?>
				</label>
				<label class="first res_block">
					<div class="border">
						<input type="text" name="captcha" autocomplete="off" value="<?php echo $captcha; ?>" placeholder="<?php echo $entry_captcha; ?>">
						<?php if ($error_captcha) { ?>
							<div class="alert alert-error"><?php echo $error_captcha; ?></div>
						<?php } ?>
					</div>	
				</label>
				<label class="last res_block">
					<div class="border captcha">
						<img class="captcha" src="index.php?route=information/contact/captcha" alt="captcha" />
					</div>
				</label>
				<div class="clear"></div>
				<input type="submit" class="button fsb" value="<?php echo $button_continue; ?>" />
			</form>
			
			<div class="clear"></div>
		</div>
	</div>
</main><!-- .content -->
<br />
<?php echo $footer; ?>