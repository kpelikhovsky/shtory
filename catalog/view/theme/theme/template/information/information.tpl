<?php echo $header; ?>

<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<h1><?php echo $heading_title; ?></h1>
			<p>
			    <?php echo $description; ?>
			</p>
		</div>
	</div>
</main>
<br />
<?php echo $footer; ?>