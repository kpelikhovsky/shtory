<?php
// News Module for Opencart v1.5.5, modified by villagedefrance (contact@villagedefrance.net)
?>

<?php echo $header; ?>

<main class="content"><?php echo $content_top; ?>
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>

			

			<?php if(isset($news_info)) {  ?>

					<h1>Новость</h1>
				
					<div class="n_date">
						<?php echo $news_info['date_added']; ?>
					</div>
				
					<div class="n_title">
						<?php echo $heading_title; ?>
					</div>
					
					<div class="n_text">
						<?php echo $description ?>
					</div>

					<div class="news" <?php if($image) { echo 'style="min-height:' . $min_height . 'px;"'; } ?>>
						<?php if ($image) { ?>
							<div class="image">
							<a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="colorbox"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a>
							</div>
						<?php } ?>
					</div>

					
<!-- AddThis Button BEGIN
					<div class="addthis">
					<?php if($addthis) { ?>
					
						<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_email"></a>
						<a class="addthis_button_print"></a>
						<a class="addthis_button_preferred_1"></a>
						<a class="addthis_button_preferred_2"></a>
						<a class="addthis_button_preferred_3"></a>
						<a class="addthis_button_preferred_4"></a>
						<a class="addthis_button_compact"></a>
						<a class="addthis_counter addthis_bubble_style"></a>
						</div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
					
					<?php } ?>
					</div>
AddThis Button END -->
				
				<div class="buttons">
					<div class="right">
						<a onclick="location='<?php echo $news; ?>'" class="button"><span><?php echo $button_news; ?></span></a>
						<a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a>
					</div>
				</div>
			<?php } elseif (isset($news_data)) { ?>
			<h1><?php echo $heading_title; ?></h1>
			<div class="text_blocks">
				<?php foreach ($news_data as $news) { ?>
					<div class="text_block text_content">
						
						<div class="div_center">

							<?php if ($news['thumb']) { ?>
								<div class="img_h">
									
									<img src="<?php echo $news['thumb']; ?>" title="<?php echo $news['title']; ?>" alt="<?php echo $news['title']; ?>" id="image" />
									
								</div>
							<?php } ?>
							<h3><?php echo $news['title']; ?></h3>
							<span class="f_date">
								<b><?php echo $text_posted; ?></b>
								<?php echo $news['posted']; ?>
							</span>
							<div class="fs14">
								<p>
									<?php echo $news['description']; ?> <br />
								</p>
								<div class="a_center">
		           					<a href="<?php echo $news['href']; ?>" class="fs14 button tt_l" >Подробнее</a>
		           				</div>
							</div>
						</div>
					</div>
			
			<?php } ?></div>
		<!-- <div class="buttons"> 
			<div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
		</div>-->
	</div>
</div>

<div class="pagination"><?php echo $pagination; ?></div>
	<?php } ?>
	<!-- <?php echo $content_bottom; ?> -->
</div>
</main><!-- .content -->
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script>

<?php echo $footer; ?>