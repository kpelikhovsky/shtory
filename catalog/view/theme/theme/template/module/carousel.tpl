<div id="slider">
    <div class="sl_container">
        <div class="sl_content">
            <?php if(!empty($banners)) foreach ($banners as $banner) { ?>
            <div class="item">
                <img src="<?php echo $banner['image']?>">
                <div class="f_title">
                    <span class="f_title_text"><?php echo $banner['title']?></span><br/>
                    <a <?php if(!preg_match('/shtorykiseya.com/', $banner['link'])) echo "target='_blank'"; ?> href="<?php echo $banner['link']?>" class="look_cat">Перейти</a>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
    <?php if(sizeof($banners) > 1) { ?>
        <a href="#" class="nav_a sl_left_nav"><span class="bg"></span></a>
        <a href="#" class="nav_a sl_right_nav"><span class="bg"></span></a>
        <div class="sl_nav">
        </div>
    <?php }?>
</div>
<script type="text/javascript"><!--
$('#slider .item').jcarousel({
    vertical: false,
    visible: <?php echo $limit; ?>,
    scroll: <?php echo $scroll; ?>
});
//--></script>