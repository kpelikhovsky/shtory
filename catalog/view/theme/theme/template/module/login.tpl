<?php if (!$this->customer->isLogged()) { ?>
<div class="box">
  <div class="box-content">
  
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_login"> 
    <span style="text-align: left;"><input type="text" name="email" placeholder="<?php echo $entry_email_address; ?>" id="login"/></span>
    <input type="password" placeholder="<?php echo $entry_password; ?>" name="password" />
    <div style="float:left; text-align: right;">
      <a href="<?php echo $this->url->link('account/login', '', 'SSL'); ?>" class="button">
        <?php echo $button_create; ?>
      </a>
    </div>
    <div style="float:right; text-align: right;"><a onclick="$('#module_login').submit();" class="button"><span><?php echo $button_login; ?></span></a></div>
    <br style="clear:both;"/>
    </form>
  </div> <!-- <div class="box-content"> -->
      <div class="t_r">
      
        <a href="<?php echo $this->url->link('account/forgotten', ''); ?>" >
          Забыли пароль?
        </a>
      
      <div class="clear"></div>
    </div>
 </div>
  <script type="text/javascript">
  $('#module_login input').keydown(function(e) 
  {
    if (e.keyCode == 13) {
      $('#module_login').submit();
    }
  });
  </script>
<?php } ?>
