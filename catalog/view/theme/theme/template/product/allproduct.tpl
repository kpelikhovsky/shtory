<?php echo $header; ?>
<?php echo $content_top; ?>

<?php echo $footer; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<?php echo $breadcrumb['separator']; ?><li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
			<?php } ?>
			
			<h1>Фотошторы</h1>
			<div class="left_menu">
				<div class="left_m menu">
					<div class="box">
						<ul class="js_menu">
							<li class="active">
								<a href="#">ФОТОШТОРЫ</a>
								<ul>
									<li><a href="#">Каталог</a></li>
									<li><a href="#">Акции</a></li>
									<li><a href="#">Акции</a></li>
									<li><a href="#">Новости</a></li>
									<li><a href="#">Оплата и доставка</a></li>
									<li><a href="#">Контакты</a></li>
								</ul>	
							</li>
							<li><a href="#">Шторы Кисея</a></li>
						</ul>
					</div>
				</div>
				<div class="banner">
					<img src="img/img5.jpg" alt=""/>
					<div class="f_title">
						<span class="blue_text">Примеры печати</span><br/>
						<span class="blue_text">на разных типах ткани</span><br/>
						
						<a href="#" class="look_cat">
							смотреть
						</a>
					</div> 
				</div>
			</div>
			<div class="items_container">
				<div class="sort in_b">
					<div class="sort_content">
						<span class="sort_title">СОРТИРОВКИ:</span>
						<label>
							<span>по типу ткани:</span>
							<select>
								<option value="1">водоотталкивающие</option>
								<option value="1">водоотталкивающие</option>
								<option value="1">водоотталкивающие</option>
								<option value="1">водоотталкивающие</option>
								<option value="1">водоотталкивающие</option>
							</select>
						</label>
						<label>
							<span>по цене:</span>
							<select>
								<option value="1">от меньшей к большей</option>
							</select>
						</label>
					</div>
				</div>
				<div class="catalog in_b">
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>	
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>
					<div class="c_item">
						<div class="c_item_content">
							<div class="imgBg">
								<a href="#">
									<img src="img/img5.jpg" alt=""/>
								</a>
							</div>
							<div class="c_desc">
								<a href="#" class="title">Автобус</a><br/>
								<span class="color">(1 цвет)</span><br/>
								<span class="price"><b>850</b> грн./пара</span>
							</div>
							<div class="but_con">
								<a href="#" class="buy"><span class="bg2"></span><span>Купить</span></a>
							</div>
						</div>
					</div>

				</div>
				<div class="c_pag in_b">
					<a href="#" >&lt;</a>
					<a href="#" class="active">1</a>
					<a href="#">2</a>
					<a href="#">3</a>
					<a href="#">4</a>
					<a href="#">&gt;</a>
				</div>
				<div class="ta_c">
					<a class="continue" href="#">Показать все товары <i class="bg bot"></i></a>
				</div>		
			</div>
			<div class="clear"></div>
		</div>

		<h2 class="car_h">Распродажа</h2>
        <div class="carusel carus">
	            	<div class="car_container">
	            		<div class="hide_items">
	            			<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">100₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>
	            			<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">200₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>
	            			<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">300₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>
	            			<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">400₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>
	            			<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">500₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>
	            				<div class="item">
	            				<div class="sm_content2">
		            				<img src="img/img1.jpg">
		            				<div class="price">
		            					<span>
			            					<span class="new_price">500₴</span><br/>
			            					<span class="old_price">700₴</span>
		            					</span>
		            				</div>
		            				<div class="item_hover">
		            					<a href="#"><span class="buy_text">Радуга<br/> горизонт фиолет</span></a><br/>
		            					<a href="#" class="buy"><span class="bg"></span><span>Купить</span></a>
		            				</div>
	            				</div>
	            			</div>

	            		</div>
	            		<div class="car_content">
	            		</div>
	            	</div>
	            	<div class="pagination">
	            		<a href="#" class="nav_a left_nav"><span class="bg"></span></a>
	            		<span class="iter">1</span><span>/</span ><span class="len">4</span>
	                	<a href="#" class="nav_a right_nav"><span class="bg"></span></a>
	            	</div>
	            	<a href="#" class="nav_a sl_left_nav"><span class="bg"></span></a>
	                <a href="#" class="nav_a sl_right_nav"><span class="bg"></span></a>
	            </div>
	         <div class="gr_text">
	         	<h2 class="car_h">Распродажа</h2>
	         	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	         </div>   	
	</div>
</main><!-- .content -->


<div id="content"><?php echo $content_top; ?>
<div class="breadcrumb">
<?php foreach ($breadcrumbs as $breadcrumb) { ?>
<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
<?php } ?>
</div>
<h1><?php echo $heading_title; ?></h1>

<?php if ($products) { ?>
<div class="product-filter">
<div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
<div class="limit"><b><?php echo $text_limit; ?></b>
  <select onchange="location = this.value;">
    <?php foreach ($limits as $limits) { ?>
    <?php if ($limits['value'] == $limit) { ?>
    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
    <?php } else { ?>
    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
    <?php } ?>
    <?php } ?>
  </select>
</div>
<div class="sort"><b><?php echo $text_sort; ?></b>
  <select onchange="location = this.value;">
    <?php foreach ($sorts as $sorts) { ?>
    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
    <?php } else { ?>
    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
    <?php } ?>
    <?php } ?>
  </select>
</div>
</div>
<div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
<div class="product-list">
<?php foreach ($products as $product) { ?>
<div>
  <?php if ($product['thumb']) { ?>
  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
  <?php } ?>
  <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
  <div class="description"><?php echo $product['description']; ?></div>
  <?php if ($product['price']) { ?>
  <div class="price">
    <?php if (!$product['special']) { ?>
    <?php echo $product['price']; ?>
    <?php } else { ?>
    <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
    <?php } ?>
    <?php if ($product['tax']) { ?>
    <br />
    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
    <?php } ?>
  </div>
  <?php } ?>
  <?php if ($product['rating']) { ?>
  <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
  <?php } ?>
  <div class="cart">
    <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
  </div>
  <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
  <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
</div>
<?php } ?>
</div>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } ?>
<?php if (!$categories && !$products) { ?>
<div class="content"><?php echo $text_empty; ?></div>
<div class="buttons">
<div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
</div>
<?php } ?>
<?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function display(view) {
if (view == 'list') {
	$('.product-grid').attr('class', 'product-list');
	
	$('.product-list > div').each(function(index, element) {
		html  = '<div class="right">';
		html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
		html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
		html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
		html += '</div>';			
		
		html += '<div class="left">';
		
		var image = $(element).find('.image').html();
		
		if (image != null) { 
			html += '<div class="image">' + image + '</div>';
		}
		
		var price = $(element).find('.price').html();
		
		if (price != null) {
			html += '<div class="price">' + price  + '</div>';
		}
				
		html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
		html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
		
		var rating = $(element).find('.rating').html();
		
		if (rating != null) {
			html += '<div class="rating">' + rating + '</div>';
		}
			
		html += '</div>';

					
		$(element).html(html);
	});		
	
	$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
	
	$.cookie('display', 'list'); 
} else {
	$('.product-list').attr('class', 'product-grid');
	
	$('.product-grid > div').each(function(index, element) {
		html = '';
		
		var image = $(element).find('.image').html();
		
		if (image != null) {
			html += '<div class="image">' + image + '</div>';
		}
		
		html += '<div class="name">' + $(element).find('.name').html() + '</div>';
		html += '<div class="description">' + $(element).find('.description').html() + '</div>';
		
		var price = $(element).find('.price').html();
		
		if (price != null) {
			html += '<div class="price">' + price  + '</div>';
		}
		
		var rating = $(element).find('.rating').html();
		
		if (rating != null) {
			html += '<div class="rating">' + rating + '</div>';
		}
					
		html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
		html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
		html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
		
		$(element).html(html);
	});	
				
	$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
	
	$.cookie('display', 'grid');
}
}

view = $.cookie('display');

if (view) {
display(view);
} else {
display('list');
}
//--></script> 
<?php echo $footer; ?>