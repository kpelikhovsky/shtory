<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<h1><?php echo $heading_title; ?></h1>
			<div class="left_menu">

				<?php echo $column_left; ?>

				<?php if(isset($banner)) { ?>
					<div class="banner">
						<div class="image"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></div>
						<div class="f_title">
							<span class="blue_text"><?php echo $banner['title']; ?></span><br/>
							
							<a href="<?php echo $banner['link']; ?>" class="look_cat">
								смотреть
							</a>
						</div>
					  	
					</div>
				<?php } ?>
			</div>
			<div class="items_container">
				<!-- <div class="sort in_b">
					<div class="sort_content">
						<span class="sort_title">Показать</span>
						<label>
							<span> 
								<select onchange="location = location + this.value;">
									<option value="&attr=">Без аттрибута</option>
								<?php foreach ($totalAttributes as $attr) { ?>
								<?php if ($attrDef == $attr['attribute_id']) { ?>
									<option value="<?php echo '&attr='.$attr['attribute_id']; ?>" selected="selected"><?php echo $attr['attribute_group'].' / '.$attr['name']; ?></option>
								<?php } else { ?>
								<option value="<?php echo '&attr='.$attr['attribute_id']; ?>"><?php echo $attr['attribute_group'].' / '.$attr['name']; ?></option>
								<?php } ?>
								<?php } ?>
								</select>
							</span>					
						</label>
						<label>
							<span><?php echo $text_sort; ?></span>
							<select class="input-large" onchange="location = this.value;">
								<?php foreach ($sorts as $sorts) { ?>
								<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
								<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
								<?php } ?>
								<?php } ?>
							</select>
						</label>
					</div>
				</div> -->
				<div class="catalog in_b">
					<?php foreach ($products as $product) { ?>
					<div class="c_item">
						<div class="c_item_content">
							<?php if ($product['thumb']) { ?>
							<div class="<?php echo $catStyle ?>"><a href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
							<?php } else { ?>
							<div class="<?php echo $catStyle ?>"><a href="<?php echo $product['href']; ?>"> <img src="catalog/view/theme/theme/image/placeholder.png" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
							<?php } ?>
							<div class="c_desc">
								<a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a><br/>
								<span class="color">(1 цвет)</span><br/>
								
								<?php if ($product['price']) { ?>
								<p class="price">
									<?php if (!$product['special']) { ?>
										<span class="price"><b><?php echo $product['price']; ?></b></span>
									<?php } else { ?>
										<span class="prices"><?php echo $product['special']; ?></span></br>
										<span class="old_prices"><?php echo $product['price']; ?></span>
									<?php } ?>
									
									<?php if ($product['tax']) { ?>
										<span class="price price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
									<?php } ?>
								</p>
								<?php } ?>
							</div>
							<div class="but_con">
								<a href="<?php echo $product['href']; ?>" class="buy"><span class="bg2" onclick="addToCart('<?php echo $product['product_id']; ?>');"></span><span><?php echo $button_cart; ?></span></a>
							</div>
							<?php echo $product['attribute']; ?>
						</div>
					</div>
					<?php } ?>
				</div>
				
				<?php if ($pagination) { ?>
					<div class="c_pag in_b">
						<?php echo $pagination; ?>
					</div>
					
					<div class="ta_c">
						<?php $min_limit = array_shift($limits); ?>
						<?php $max_limit = array_pop($limits); ?>
						<?php if ($limit == $max_limit['value']) { ?>
							<a class="continue" href="<?php echo $min_limit['href']; ?>">Cвернуть <i class="bg bot"></i></a>
						<?php } else { ?>
							<a class="continue" href="<?php echo $max_limit['href']; ?>">Показать все товары <i class="bg bot"></i></a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>

		<?php echo $content_bottom; ?>

		<div class="gr_text">
	    	<h2 class="car_h"><?php echo $heading_title; ?></h2>
	   		<?php echo $description; ?>
	    </div>
	</div>
</main><!-- .content -->

<?php echo $footer; ?>
