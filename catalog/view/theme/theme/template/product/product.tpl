<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<h1><?php echo $heading_title; ?></h1>
			<div class="blocks">
				<?php if ($thumb || $images) { ?>
				<div class="res_block">
					<?php if ($thumb) { ?>
					<div class="imgCon">
						<a href="<?php echo $popup; ?>" class="gallery" rel="group" title="<?php echo $heading_title; ?>">
							<img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
						</a>
					</div>
					<?php } ?>

					<?php if ($images) { ?>
					<div class="carusel small_car">
						<div class="car_container">
							<div class="hide_items">
								<?php foreach ($images as $image) { ?>
								<div class="item">
									<div class="sm_content">
										<a href="<?php echo $image['popup']; ?>" class="gallery" rel="group" title="<?php echo $heading_title; ?>">
											<img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
										</a>
									</div>
								</div>
								<?php } ?>
							</div>
							<div class="car_content"></div>
						</div>
						<div class="pagination">
							<a href="#" class="nav_a left_nav"><span class="bg"></span></a>
							<span class="iter">1</span><span>/</span ><span class="len"><?php echo count($images);?></span>
							<a href="#" class="nav_a right_nav"><span class="bg"></span></a>
						</div>
						<a href="#" class="nav_a sl_left_nav"><span class="bg"></span></a>
						<a href="#" class="nav_a sl_right_nav"><span class="bg"></span></a>
					</div>
					<?php } ?>

					<script type="text/javascript">(function() {
					  if (window.pluso)if (typeof window.pluso.start == "function") return;
					  if (window.ifpluso==undefined) { window.ifpluso = 1;
					    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
					    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
					    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
					    var h=d[g]('body')[0];
					    h.appendChild(s);
					  }})();
					</script>
					<div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=05" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email"></div>

				</div>
				<?php } ?>

				<div class="res_block product-info">
					<?php if ($sku) { ?>
						<div class="articul">Артикул: <?php echo $sku;?></div>
					<?php } ?>
					<!-- Price -->
					<?php if ($price) { ?>
						<?php if (!$special) { ?>
							<div class="prices in_b">
								<span><?php echo $price; ?></span>
							</div>
						<?php } else { ?>
							<div class="prices in_b">
								<span><?php echo $special; ?></span>
							</div>
							<div class="prices old_prices in_b">
								<span><?php echo $price; ?></span>
							</div>
						<?php } ?>

						<?php if ($tax) { ?>
							<div class="prices in_b">
								<span><?php echo $text_tax; ?> <?php echo $tax; ?></span>
							</div>
						<?php } ?>

						<?php if ($points) { ?>
							<div class="prices in_b">
								<span><?php echo $text_points; ?> <?php echo $points; ?></span>
							</div>
						<?php } ?>

						<?php if ($discounts) { ?>
							<br />
							<?php foreach ($discounts as $discount) { ?>
								<div class="prices in_b">
									<span><?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?></span>
								</div>
							<?php } ?>
						<?php } ?>
						
						<div class="i_text"></div>
					<?php } ?>
					
					<?php if(strlen($stock) < 10) {?>
					<div class="in_b" style="margin-bottom:30px;">
						<div class="quan">
							<input name="quantity" type="text" size="2" value="<?php echo $minimum; ?>" autocomplete="off">
							<a href="#" class="tp"></a>
							<a href="#" class="bt"></a>
						</div>
						<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
						<a href="#" class="buy button torec va_m" id="button-cart" ><span class="bg"></span><span>В корзину</span></a>
					</div>
					<?php } else { echo "<h2 class='h_con'>$stock</h2>"; } ?>

					<h2 class="h_con">Описание</h2>
					
					<div class="i_text">
						<?php if ($manufacturer) { ?>
							<span class="cb"><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
						<?php } ?>
						<?php if ($width && $height) { ?>
							<span class="cb">Размеры одного полотна:</span> ширина <?php echo $width; ?>, высота <?php echo $height; ?><br />
						<?php } ?>
						<?php if ($weight) { ?>
							<span class="cb">Вес одного полотна:</span> <?php echo $weight; ?><br />
						<?php } ?>
						<?php if ($attribute_groups) { ?>
							<?php foreach ($attribute_groups as $attribute_group) { ?>
								<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
									<span class="cb"><?php echo $attribute['name']; ?>:</span> <?php echo $attribute['text']; ?><br/>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<br />
						<?php echo $description; ?>
					</div>
					<?php if ($options) { ?>
					<div class="i_text">
						<hr>
						<h3><?php echo $text_option; ?></h3>
						<?php foreach ($options as $option) { ?>
							<?php if ($option['type'] == 'select') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<li>
										<select class="input-block-level" name="option[<?php echo $option['product_option_id']; ?>]">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach ($option['option_value'] as $option_value) { ?>
											<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
											<?php if ($option_value['price']) { ?>
											(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
											<?php } ?>
											</option>
											<?php } ?>
										</select>
									</li>
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'radio') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<?php foreach ($option['option_value'] as $option_value) { ?>
									<li>
										<label class="radio" for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
											<?php if ($option_value['price']) { ?>
											(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
											<?php } ?>
										</label>
									</li>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'checkbox') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<?php foreach ($option['option_value'] as $option_value) { ?>
									<li>
										<label class="checkbox" for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
											<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
											<?php if ($option_value['price']) { ?>
											(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
											<?php } ?>
										</label>
									</li>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'image') { ?>
							<div class="control-group">
								<ul class="unstyled option option-image" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<?php foreach ($option['option_value'] as $option_value) { ?>
									<input class="pull-left" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
									<label class="pull-left" for="option-value-<?php echo $option_value['product_option_value_id']; ?>"> <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /> </label>
									<label class="pull-left" for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
										<?php if ($option_value['price']) { ?>
										(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
										<?php } ?>
									</label>
									<div class="clearfix"></div>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'text') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<input class="input-block-level" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'textarea') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<textarea class="input-block-level" name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'file') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> 
									</li>
									<li>Файл не выбран</li>
									<input type="button" class="btn" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>">
									<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'date') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<input class="input-block-level date" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'datetime') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<input class="input-block-level datetime" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
								</ul>
							</div>
							<?php } ?>
							<?php if ($option['type'] == 'time') { ?>
							<div class="control-group">
								<ul class="unstyled option" id="option-<?php echo $option['product_option_id']; ?>">
									<li>
										<?php if ($option['required']) { ?>
										<span class="text-error">*</span>
										<?php } ?>
										<?php echo $option['name']; ?> </li>
									<input class="input-block-level time" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
								</ul>
							</div>
							<?php } ?>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
				<?php if ($review_status) { ?>
				<!-- Tab - Reviews -->
				<div class="clear"></div>
				<div class="ci_title">Отзывы о товаре</div>
				<div class="size_com">
					(<?php echo $reviews_count;?> <?php echo pluralForm($reviews_count, 'отзыв', 'отзыва', 'отзывов') ?>)
				</div>

				<div id="reviews"></div>
			
				<div class="clear" style="height:10px;"></div>
				<div class="con_form">
					<h2 class="h_con"><?php echo $text_write; ?></h2>
					<div class="clear" style="height:10px;"></div>
					<label class="first res_block">
						<div class="border">
							<input type="text" name="name" placeholder="<?php echo $entry_name; ?>">
						</div>
					</label>
					<label class="last res_block">
						<div class="border">
							<b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
							<input type="radio" name="rating" value="1" />
							&nbsp;
							<input type="radio" name="rating" value="2" />
							&nbsp;
							<input type="radio" name="rating" value="3" checked="checked" />
							&nbsp;
							<input type="radio" name="rating" value="4" />
							&nbsp;
							<input type="radio" name="rating" value="5" />
							&nbsp;<span><?php echo $entry_good; ?></span>
						</div>
					</label>
					<label class="first res_block">
						<div class="border">
							<input type="text" autocomplete="off" name="captcha" placeholder="<?php echo $entry_captcha; ?>" value="" />
						</div>
					</label>
					<label class="last res_block">
						<div class="border captcha">
							<img src="index.php?route=product/product/captcha" alt="" id="captcha" />
						</div>

					</label>
					<div class="clear"></div>
					<label class="border">
						<textarea name="text" placeholder="<?php echo $entry_review; ?>" style="resize: vertical;"></textarea>
					</label>
					<input type="submit" id="button-review" class="button" value="<?php echo $button_continue; ?>"/>
				</div>
				<?php } ?>
			
				<?php if ($tags) { ?>
					<div class="clear"></div>
					<br />
					<div class="tags"><?php echo $text_tags; ?>
						<?php for ($i = 0; $i < count($tags); $i++) { ?>
							<?php if ($i < (count($tags) - 1)) { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
							<?php } else { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
							<?php } ?>
						<?php } ?>
					</div>
				<?php } ?>
				
				<div class="clear"></div>
				<!-- Related products -->
				<?php if ($products) { ?>
				<div class="ci_title">Похожие товары</div>
				<div class="carusel carus">
					<div class="car_container">
						<div class="hide_items">
							<?php foreach ($products as $product) { ?>
							<div class="item">
								<div class="sm_content2">
									<?php if ($product['thumb']) { ?>
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
									<?php } else { ?>
										<img src="catalog/view/theme/default/image/placeholder.png" alt="<?php echo $product['name']; ?>" />
									<?php } ?>
									<?php if ($product['price']) { ?>
										<div class="price">
											<span>
												<?php if (!$product['special']) { ?>
												<span class="new_price"><?php echo $product['price']; ?></span>
												<?php } else { ?>
													<span class="new_price"><?php echo $product['special']; ?></span><br/>
													<span class="old_price"><?php echo $product['price']; ?></span>
												<?php } ?>
											</span>
										</div>
									<?php } ?>
									<div class="item_hover">
										<a href="<?php echo $product['href']; ?>"><span class="buy_text"><?php echo $product['name']; ?></span></a><br/>
										<a href="<?php echo $product['href']; ?>" class="buy" onclick="addToCart('<?php echo $product['product_id']; ?>');"><span class="bg"></span><span>Купить</span></a>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="car_content"></div>
					</div>
					<div class="pagination">
						<a href="#" class="nav_a left_nav"><span class="bg"></span></a>
						<span class="iter">1</span><span>/</span ><span class="len"><?php echo count($products); ?></span>
						<a href="#" class="nav_a right_nav"><span class="bg"></span></a>
					</div>
					<a href="#" class="nav_a sl_left_nav"><span class="bg"></span></a>
					<a href="#" class="nav_a sl_right_nav"><span class="bg"></span></a>
				</div>
				<div class="clear"></div>
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</main><!-- .content -->
<br />
<script type="text/javascript"><!--
$('#button-cart').click(function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json'
	})
		.always(function (json) {
			$('.alert-success, .alert-warning, .attention, .information, .alert-error').remove();

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<div class="error">' + json['error']['option'][i] + '</div>');
					}
				}
			}

			if (json['success']) {
				$('#notification').html('<div class="alert alert-success" style="display: none;"><h3>' + json['success'] + '</h3></div>');
				$('.alert-success').fadeIn('slow');

				$('.s_title > span').html(json['num']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		});

	return false;
});
--></script>

<script type="text/javascript"><!--
	$('#reviews .c_pag a').live('click', function() {
		$('#reviews').load(this.href);
		return false;
	});

	$('#reviews').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

	$('#button-review').bind('click', function() {
		$.ajax({
			url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
			type: 'post',
			dataType: 'json',
			data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
			beforeSend: function() {
				$('#button-review').prop('disabled', true);
			},
			complete: function() {
				$('#button-review').prop('disabled', false);
			},
			success: function(data) {
				if (data['error']) {
					alert(data['error']);
				}

				if (data['success']) {
					alert(data['success']);

					$('input[name=\'name\']').val('');
					$('textarea[name=\'text\']').val('');
					$('input[name=\'rating\']:checked').prop('checked', '');
					$('input[name=\'captcha\']').val('');
				}
			}
		});
	});
//--></script>

<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
			$('#option-<?php echo $option['product_option_id']; ?> >li:nth-child(2)').text("Выбран файл: " + file);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>

<?php echo $footer; ?>