<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="comment" title="Рейтинг: <?php echo $review['rating']; ?>">
	<div class="name"><span class="cb"><?php echo $review['author']; ?></span>, <?php echo $review['date_added']; ?></div>
	<p><?php echo $review['text']; ?></p>
</div>
<?php } ?>
<div class="c_pag in_b"><?php echo $pagination; ?></div>
<?php } ?>
