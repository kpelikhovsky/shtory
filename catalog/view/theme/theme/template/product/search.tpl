<?php echo $header; ?>

<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>
			<h1><?php echo $heading_title; ?></h1>
		
			<div class="left_menu">
				<h2 class="h_con"><?php echo $text_critea; ?></h2>
				<div class="clear" style="height:10px;"></div>
				<div class="con_form">
					<label class="first" for="search">
						<div class="border">
							<?php if ($search) { ?>
								<input type="text" name="search" value="<?php echo $search; ?>" />
							<?php } else { ?>
								<input type="text" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
							<?php } ?>
						</div>
					</label>
					
					<label class="first" for="category_id">
						<div class="border">
							<select name="category_id">
								<option value="0"><?php echo $text_category; ?></option>
								<?php foreach ($categories as $category_1) { ?>
									<?php if ($category_1['category_id'] == $filter_category_id) { ?>
										<option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
									<?php } ?>
								
									<?php foreach ($category_1['children'] as $category_2) { ?>
										<?php if ($category_2['category_id'] == $filter_category_id) { ?>
											<option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
										<?php } else { ?>
											<option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
										<?php } ?>
								
										<?php foreach ($category_2['children'] as $category_3) { ?>
											<?php if ($category_3['category_id'] == $filter_category_id) { ?>
												<option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
											<?php } else { ?>
												<option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</label>
				
					<label class="first" for="sub_category">
						<div class="border checkbox">
							<input type="checkbox" name="sub_category" value="1" id="sub_category"<?php if ($sub_category) { ?> checked="checked"<?php } ?> />
							<?php echo $text_sub_category; ?>
						</div>
					</label>

					<label class="first" for="description">
						<div class="border checkbox">
							<input type="checkbox" name="description" value="1" id="description"<?php if ($description) { ?> checked="checked"<?php } ?> />
							<?php echo $entry_description; ?>
						</div>
					</label>

					<input type="button" class="button btn" id="button-search" value="<?php echo $button_search; ?>" />
				</div>
			</div>
			
			<?php if ($products) { ?>
				<div class="items_container">
					<!-- <div class="sort in_b">
						<div class="sort_content">
							<span class="sort_title">СОРТИРОВКИ:</span>
							<label>
								<span>по типу ткани:</span>
								<select>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
								</select>
							</label>
							<label>
								<span><?php echo $text_sort; ?></span>
								<select class="input-large" onchange="location = this.value;">
									<?php foreach ($sorts as $sorts) { ?>
									<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
									<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
									<?php } else { ?>
									<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
									<?php } ?>
									<?php } ?>
								</select>
							</label>
						</div>
					</div> -->
					<div class="catalog in_b">
						<?php foreach ($products as $product) { ?>
						<div class="c_item">
							<div class="c_item_content">
								<?php if ($product['thumb']) { ?>
								<div class="imgBg"><a href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
								<?php } else { ?>
								<div class="imgBg"><a href="<?php echo $product['href']; ?>"> <img src="catalog/view/theme/theme/image/placeholder.png" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
								<?php } ?>
								<div class="c_desc">
									<a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a><br/>
									<span class="color">(1 цвет)</span><br/>
									
									<?php if ($product['price']) { ?>
									<p class="price">
										<?php if (!$product['special']) { ?>
											<span class="price"><b><?php echo $product['price']; ?></b></span>
										<?php } else { ?>
											<span class="prices in_b"><?php echo $product['special']; ?></span>
											<span class="prices old_prices in_b"><?php echo $product['price']; ?></span>
										<?php } ?>
										
										<?php if ($product['tax']) { ?>
											<span class="price price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
										<?php } ?>
									</p>
									<?php } ?>
								</div>
								<div class="but_con">
									<a href="#" class="buy" onclick="addToCart('<?php echo $product['product_id']; ?>');"><span class="bg2"></span><span><?php echo $button_cart; ?></span></a>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>

					<?php if ($pagination) { ?>
						<div class="c_pag in_b">
							<?php echo $pagination; ?>
						</div>
						
						<div class="ta_c">
							<?php $min_limit = array_shift($limits); ?>
							<?php $max_limit = array_pop($limits); ?>
							<?php if ($limit == $max_limit['value']) { ?>
								<a class="continue" href="<?php echo $min_limit['href']; ?>">Cвернуть <i class="bg bot"></i></a>
							<?php } else { ?>
								<a class="continue" href="<?php echo $max_limit['href']; ?>">Показать все товары <i class="bg bot"></i></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			<?php } else { ?>
			<div class="items_container">
				<div class="message">
					<p><?php echo $text_empty; ?></p>
					<div class="buttons">
						<a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a>
					</div>
				</div>
			</div>
			<?php } ?>

			<div class="clear"></div>
		</div>
		<?php echo $content_bottom;?>
	</div>
</main>
<br />
<script type="text/javascript"><!--
	$('.content input[name=\'search\']').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#button-search').trigger('click');
		}
	});

	$('select[name=\'category_id\']').change(function() {
		if (this.value == '0') {
			$('input[name=\'sub_category\']').prop('disabled', 'disabled');
			$('input[name=\'sub_category\']').removeProp('checked');
		} else {
			$('input[name=\'sub_category\']').removeProp('disabled');
		}
	});

	$('select[name=\'category_id\']').trigger('change');

	$('#button-search').click(function() {
		url = 'index.php?route=product/search';

		var search = $('.content input[name=\'search\']').prop('value');

		if (search) {
			url += '&search=' + encodeURIComponent(search);
		}

		var category_id = $('.content select[name=\'category_id\']').prop('value');

		if (category_id > 0) {
			url += '&filter_category_id=' + encodeURIComponent(category_id);
		}

		var sub_category = $('.content input[name=\'sub_category\']:checked').prop('value');

		if (sub_category) {
			url += '&sub_category=true';
		}

		var filter_description = $('.content input[name=\'description\']:checked').prop('value');

		if (filter_description) {
			url += '&description=true';
		}

		location = url;
	});
--></script>
<?php echo $footer; ?>