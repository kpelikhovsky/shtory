<?php echo $header; ?>
<main class="content">
	<div class="box" id="main_box">
		<div class="grey_box">
			<ul class="in_b bread" >
				<?php $last_breadcrumb = array_pop($breadcrumbs); ?>
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="bg"></span></li>
				<?php } ?>
				<li><span><?php echo $last_breadcrumb['text'];?></span></li>
			</ul>

			<h1><?php echo $heading_title; ?></h1>

			<?php if ($products) { ?>
				<div class="box">
					<!-- <div class="sort in_b">
						<div class="sort_content">
							<span class="sort_title">СОРТИРОВКИ:</span>
							<label>
								<span>по типу ткани:</span>
								<select>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
									<option value="1">водоотталкивающие</option>
								</select>
							</label>
							<label>
								<span><?php echo $text_sort; ?></span>
								<select class="input-large" onchange="location = this.value;">
									<?php foreach ($sorts as $sorts) { ?>
									<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
									<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
									<?php } else { ?>
									<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
									<?php } ?>
									<?php } ?>
								</select>
							</label>
						</div>
					</div> -->
					<div class="catalog in_b">
						<?php foreach ($products as $product) { ?>
						<div class="c_item">
							<div class="c_item_content">
								<?php if ($product['thumb']) { ?>
								<div class="imgBg"><a href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
								<?php } else { ?>
								<div class="imgBg"><a href="<?php echo $product['href']; ?>"> <img src="catalog/view/theme/theme/image/placeholder.png" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /> </a> </div>
								<?php } ?>
								<div class="c_desc">
									<a href="<?php echo $product['href']; ?>" class="title"><?php echo $product['name']; ?></a><br/>
									<span class="color">(1 цвет)</span><br/>
									
									<?php if ($product['price']) { ?>
									<p class="price">
										<?php if (!$product['special']) { ?>
											<span class="price"><b><?php echo $product['price']; ?></b></span>
										<?php } else { ?>
											<span class="new_price"><?php echo $product['special']; ?></span>
											<span class="old_price"><strike><?php echo $product['price']; ?></strike></span>
										<?php } ?>
										
										<?php if ($product['tax']) { ?>
											<span class="price price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
										<?php } ?>
									</p>
									<?php } ?>
								</div>
								<div class="but_con">
									<a href="#" class="buy" onclick="addToCart('<?php echo $product['product_id']; ?>');"><span class="bg2"></span><span><?php echo $button_cart; ?></span></a>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>

					<?php if ($pagination) { ?>
						<div class="c_pag in_b">
							<?php echo $pagination; ?>
						</div>
						
						<div class="ta_c">
							<?php $min_limit = array_shift($limits); ?>
							<?php $max_limit = array_pop($limits); ?>
							<?php if ($limit == $max_limit['value']) { ?>
								<a class="continue" href="<?php echo $min_limit['href']; ?>">Cвернуть <i class="bg bot"></i></a>
							<?php } else { ?>
								<a class="continue" href="<?php echo $max_limit['href']; ?>">Показать все товары <i class="bg bot"></i></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			<?php } else { ?>
				<div class="message">
					<p><?php echo $text_empty; ?></p>
					<div class="buttons">
						<a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a>
					</div>
				</div>
			<?php } ?>
			<div class="clear"></div>
		</div>
		
		<?php echo $content_bottom; ?>
	</div>
</main>
<br />
<?php echo $footer; ?>